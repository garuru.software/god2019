﻿//------------------------------------------------
//
//
// 
//
//
//------------------------------------------------
class CWazaCommand;
class CSikakuPlayer : public CAction
{
	enum {
	//	enStatNormal,
		enStatGround,
		enStatAir,
		enStatLanding,
		enStatFrontDash,
		enStatBackDash,

		enStatKick,
		enStatPunch,

		enStatAsibarai,
		enStatBodyBrow,

		enStatThrowSukari,
		enStatThrowAtari,
		enStatThrowKurai,

		enStatThrowKuchuAtari,
		enStatThrowKuchuKurai,

		enStatHadouken,
		enStatSyouryuken,
		enStatSenpukyaku,
		enStatSinkuHadoken,
		enStatKuchuTatsumaki,

		enStatDamageGround,
		enStatDamageAir,
		enStatDamageFuttobi,
		enStatDamageDown,
		enStatDamageDead,

		enStatGetUp,
		enStatStan,

		enStatWinner,

	};

public:

	CSikakuPlayer( Sint32 sPlayer );
	~CSikakuPlayer();
	void SeqMain();
	void SeqCrash();
	void Draw();

	void SetEnemy( CSikakuPlayer *pEnemy )
	{
		m_pEnemy = pEnemy;
	}

	gxPos* GetPos()
	{
		return &m_Pos;
	}

	void SetKabegiwa( gxBool bFlag )
	{
		m_bKabegiwa = bFlag;
	}

	gxBool IsKabegiwa()
	{
		return m_bKabegiwa;
	}

	Sint32 GetComboCnt()
	{
		return m_sComboCnt;
	}

	Sint32 GetHP()
	{
		return m_sHP;
	}

	gxBool IsCBMaximum()
	{
		if( m_sCB >= 1000 ) return gxTrue;

		return gxFalse;
	}

	void AddCB( Sint32 n )
	{
		m_sCB += n;
		if( m_sCB >= 1000 ) m_sCB = 1000;
	}

	Sint32 GetCB()
	{
		return m_sCB;
	}

	gxBool IsDead()
	{
		if( m_sHP <= -60 )
		{
			return gxTrue;
		}
		return gxFalse;
	}

	gxBool IsExist()
	{
		return m_bExist;
	}

	void Stop()
	{
		m_bActive = gxFalse;
	}

	void Go()
	{
		m_bActive = gxTrue;
	}

	void SetWinner()
	{
		changeStat( enStatWinner );
	}

	void SetThrow( Sint32 n = enStatThrowKurai )
	{
		controlReset();

		changeStat( n );
	}

	gxBool IsNageable()
	{
		return m_bNagerare;
	}

private:

	void changeStat( Sint32 stat )
	{
		m_sAnimCnt = 0;
		m_bFirstAction = gxTrue;
		m_sStat = stat;
		m_sAtackWait = 0;
		m_bReversal = gxFalse;
	}

	void controlMove();
	void controlJump();
	void controlLanding();
	void controlFrontDash();
	void controlBackDash();

	void controlStandPuch();
	void controlStandKick();

	void controlLowPunch();
	void controlAsibarai();


	void controlJumpKick();
	void controlJumpKickYoko();

	void controlThrowSukari();
	void controlThrowAtari();
	void controlThrowKurai();

	void controlThrowKuchuAtari();
	void controlThrowKuchuKurai();

	void controlDamageGround();
	void controlDamageAir();
	void controlDamageFuttobi();
	void controlDamageDead();
	void controlDamageDown();
	void controlGetup();
	void controlStan();

	void controlWinner();

	void controlSyouryuken();
	void controlHadouken();
	void controlSenpukyaku();
	void controlSinkuHadouken();
	void controlKuchuTatsumaki();

	gxBool jumpNow();
	void atariJudge();
	void setKurai( Sint32 u , Sint32 v , Sint32 w, Sint32 h );
	void setAtari( Sint32 u , Sint32 v , Sint32 w, Sint32 h ,Sint32 type = 0 , Sint32 sDir = 0 );

	gxBool hissatsuCheck();

	void controlReset()
	{
		m_Punch.x = 0;
		m_Punch.y = 0;
		m_sJumpCnt = 0;
		m_sAnimCnt = 0;
		m_bJumpKick = gxFalse;
		m_bReversal = gxFalse;
	}

	Sint32 JoyPush()
	{
		if( !m_bActive ) return 0x00000000;

		return gxLib::Joy(m_sPlayer)->psh;
	}

	Sint32 JoyTrig()
	{
		if( !m_bActive ) return 0x00000000;

		return gxLib::Joy(m_sPlayer)->trg;
	}


	void atackEffect( Sint32 x , Sint32 y );

	gxPos m_Pos;
	gxPos m_Sit;
	gxPos m_Add;
	gxPos m_Punch;
	gxPos m_Hand;
	Float32 m_fHanaRot;

	Sint32 m_sStat;
	Sint32 m_sJumpCnt;
	Sint32 m_sAnimCnt;
	Sint32 m_sDir;
	Sint32 m_sJumpDir;
	Sint32 m_sDirPos;
	Sint32 m_sDamageWait;
	Sint32 m_sHitStopFrm;
	Sint32 m_sAtackWait;
	Sint32 m_sKnockBack;

	gxBool m_bAtackHit;
	gxBool m_bJump;
	gxBool m_bCrouch;
	gxBool m_bJumpKick;
	gxBool m_bFirstAction;
	gxBool m_bReversal;

	gxBool m_bKabegiwa;
	gxBool m_bNagerare;


	Sint32 m_sAtackType;

	Sint32 m_sPlayer;
	Sint32 m_sComboCnt;
	Sint32 m_sReversalCnt;
	Sint32 m_sStanDamage;
	Sint32 m_sStanWait;

	Sint32 m_sHP;
	Sint32 m_sCB;


	Float32 m_fRotation;

	CWazaCommand *m_pCmd;

	CSikakuPlayer *m_pEnemy;

	CCollision m_Atari;
	CCollision m_Kurai;

	gxBool m_bExist;

	gxBool m_bActive;
};


