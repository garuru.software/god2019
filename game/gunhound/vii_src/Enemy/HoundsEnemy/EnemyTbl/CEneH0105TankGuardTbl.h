enum {
 enTexPageBase = enTexCEneH0105TankGuard,
};
gxSprite SprCEneH0105TankGuard[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,0,104,56,24,32,16},//砲台回転１
    {enTexPageBase+0,0,136,56,24,32,16},//砲台回転２
    {enTexPageBase+0,0,168,64,24,32,16},//砲台回転３
    {enTexPageBase+0,0,200,104,24,72,24},//砲台
    {enTexPageBase+0,0,232,112,24,72,8},//胴体
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneH0105TankGuard[][16]={
    {160,200,0,0,0,0,100,100,100,0},//シーン
    {5,-18,0,0,0,0,100,100,100,5},//胴体
    {0,0,0,0,0,0,100,100,100,4},//砲台
    {0,0,0,0,0,0,0,0,0,6},//0
};
enum {
    enPARENT,
    enDOUTAI,
    enHOUDAI,
    enMax,
    
};
Sint32 m_sParentCEneH0105TankGuard[]={
    -1,
    enPARENT,
    enDOUTAI,
    
};
