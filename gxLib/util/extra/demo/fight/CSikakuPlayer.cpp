﻿//------------------------------------------------
//
// プレイヤールーチン
// 
// written by tomi.2013.01.18
//
//------------------------------------------------
#include <gxLib.h>
#include <gxLib/Util/extra/action/CWazaCommand.h>
#include <gxLib/Util/extra/action/CCollisionManager.h>
#include "sikaku.h"

#define UNDERLINE (260)
Uint32 uCmdHadouken[]={
	CWazaCommand::enKeyDown,
	CWazaCommand::enKeyRightDown,
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyBtnA,
	CWazaCommand::enKeyEnd,
};

Uint32 uCmdDash[]={
	CWazaCommand::enKeyLeverNone,
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyLeverNone,
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyEnd,
};
Uint32 uCmdBack[]={
	CWazaCommand::enKeyLeverNone,
	CWazaCommand::enKeyLeft,
	CWazaCommand::enKeyLeverNone,
	CWazaCommand::enKeyLeft,
	CWazaCommand::enKeyEnd,
};
Uint32 uCmdSyouryuken[]={
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyDown,
	CWazaCommand::enKeyRightDown,
	CWazaCommand::enKeyBtnA,
	CWazaCommand::enKeyEnd,
};
Uint32 uCmdSenpukyaku[]={
	CWazaCommand::enKeyDown,
	CWazaCommand::enKeyDownLeft,
	CWazaCommand::enKeyLeft,
	CWazaCommand::enKeyBtnA,
	CWazaCommand::enKeyEnd,
};

Uint32 uCmdSinkuHadouken[]={
	CWazaCommand::enKeyDown,
	CWazaCommand::enKeyRightDown,
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyDown,
	CWazaCommand::enKeyRightDown,
	CWazaCommand::enKeyRight,
	CWazaCommand::enKeyBtnA,
	CWazaCommand::enKeyEnd,
};

enum {
	enHitStopFrm = 12,
	enNageMaai   = 42,
	enStanDamage = 50,
};

CSikakuPlayer::CSikakuPlayer( Sint32 sPlayer)
{
	//------------------------------------------------
	//------------------------------------------------

	if( sPlayer == 0 )
	{
		m_sDir = DIR_RIGHT;
	}
	else
	{
		m_sDir = DIR_LEFT;
	}

	m_sDirPos = m_sDir*100;

	m_Pos.x = _COD(enStageWidth/2 - m_sDir*96 );
	m_Pos.y = _COD( UNDERLINE );

	m_Add.x = 0;
	m_Add.y = 0;

	m_Sit.x = 0;
	m_Sit.y = 0;

	m_Punch.x = 0;
	m_Punch.y = 0;

	m_Hand.x = 0;
	m_Hand.y = 0;

	m_fHanaRot = 0.f;

	m_pCmd = new CWazaCommand();

	m_sPlayer = sPlayer;

	m_bJump = gxFalse;
	m_sJumpCnt = 0;

	m_bCrouch = gxFalse;

	m_sStat = 0;

	m_sAnimCnt = 0;
	m_bJumpKick = gxFalse;
	m_sAtackType = 0;
	m_bFirstAction = gxTrue;
	m_sDamageWait = 0;
	m_sHitStopFrm = 0;
	m_bAtackHit = gxFalse;
	m_bKabegiwa = gxFalse;
	m_sKnockBack = 0;
	m_bNagerare = gxTrue;

	m_sComboCnt = 0;
	m_sReversalCnt = 100;
	m_sStanDamage = 0;
	m_sStanWait   = 0;
	m_bReversal = gxFalse;

	m_sHP = 1000;
	m_sCB = 1000;

	m_bActive = gxFalse;

	m_bExist = gxTrue;

	m_fRotation = 0.f;

}


CSikakuPlayer::~CSikakuPlayer()
{
	//------------------------------------------------
	//------------------------------------------------
	
	delete m_pCmd;
}

void CSikakuPlayer::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------
	if( CSikakuGameMain::GetInstance()->IsTheWorld( ) )
	{
		return;
	}

	m_pCmd->SetLog( JoyPush() );

	m_bCrouch = gxFalse;

	atariJudge();

	if( m_sDamageWait > 0 )
	{
		m_sDamageWait --;
	}
	if( m_sHitStopFrm > 0 )
	{
		m_sHitStopFrm --;
	}
	if( m_sHitStopFrm > 0 || m_sDamageWait > 0)
	{
		return;
	}

	m_bNagerare = gxTrue;

	switch( m_sStat ){
//	case enStatNormal:
//		break;

	case enStatGround:
		m_sComboCnt = 0;
		controlMove();
		break;

	case enStatAir:
		controlJump();
		break;

	case enStatLanding:
		controlLanding();
		break;

	case enStatFrontDash:
		controlFrontDash();
		break;

	case enStatBackDash:
		controlBackDash();
		break;

	case enStatPunch:
		controlStandPuch();
		break;

	case enStatKick:
		controlStandKick();
		break;

	case enStatBodyBrow:
		controlLowPunch();
		break;

	case enStatAsibarai:
		controlAsibarai();
		break;

	case enStatThrowSukari:
		controlThrowSukari();
		break;
	case enStatThrowAtari:
		m_bNagerare = gxFalse;
		controlThrowAtari();
		break;

	case enStatThrowKurai:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlThrowKurai();
		break;

	case enStatThrowKuchuAtari:
		controlThrowKuchuAtari();
		break;

	case enStatThrowKuchuKurai:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlThrowKuchuKurai();
		break;

	case enStatDamageGround:
		m_bNagerare = gxFalse;
		controlDamageGround();
		break;

	case enStatDamageAir:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlDamageAir();
		break;

	case enStatHadouken:
		controlHadouken();
		break;

	case enStatSyouryuken:
		controlSyouryuken();
		break;

	case enStatSenpukyaku:
		controlSenpukyaku();
		break;

	case enStatSinkuHadoken:
		controlSinkuHadouken();
		break;

	case enStatKuchuTatsumaki:
		controlKuchuTatsumaki();
		break;

	case enStatDamageFuttobi:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlDamageFuttobi();
		break;

	case enStatDamageDead:
		m_bNagerare = gxFalse;
		m_sComboCnt = 0;
		controlDamageDead();
		break;

	case enStatDamageDown:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlDamageDown();
		break;

	case enStatGetUp:
		m_sComboCnt = 0;
		m_bNagerare = gxFalse;
		controlGetup();
		break;

	case enStatStan:
		m_sComboCnt = 0;
		controlStan();
		break;

	case enStatWinner:
		m_bNagerare = gxFalse;
		controlWinner();
		break;

	default:
		{
			Sint32 n = 0;
			n ++;
		}
		break;
	}

	if( ABS( m_Pos.x - m_pEnemy->GetPos()->x ) > enRingWidth*100 )
	{
		if( m_Pos.x < m_pEnemy->GetPos()->x )
		{
			m_Pos.x = m_pEnemy->GetPos()->x - enRingWidth*100;
		}
		else
		{
			m_Pos.x = m_pEnemy->GetPos()->x + enRingWidth*100;
		}
	}

	if( m_sKnockBack )
	{
		m_Pos.x += m_sKnockBack;
		if( m_sKnockBack < 0 ) m_sKnockBack ++;
		if( m_sKnockBack > 0 ) m_sKnockBack --;
		m_sKnockBack += -m_sKnockBack/10;
	}

	m_sDirPos += ( m_sDir*100 - m_sDirPos )/5;

	m_sAnimCnt ++;
	m_sReversalCnt ++;

	if( m_sStanDamage > 0 )
	{
		m_sStanDamage -= 1;
		if( m_sStanDamage < 0 )
		{
			m_sStanDamage = 0;
		}
	}

	if( m_sStanWait > 0 )
	{
		new CEffAtack( m_Pos.x + gxUtil::Cos( (m_sStanWait*4)%360 )*5600 , m_Pos.y - 6400 + gxUtil::Sin( (m_sStanWait*4)%360 )*1600 , 1 );
		m_sStanWait ++;
	}

}


void CSikakuPlayer::setKurai( Sint32 u , Sint32 v , Sint32 w, Sint32 h )
{
	EnCollisionID id;
	Sint32 sx = m_Pos.x/100;
	Sint32 sy = m_Pos.y/100;
	Sint32 sw=0,sh=0;
	m_Kurai.m_sHp = 100;

	if( u == 0 && v == 0 && w == 0 && h == 0 )
	{
		//標準の当たり判定
		u = -16;
		v = -48+ 32*m_Sit.y/100;
		w = 32;
		h = 64- 16*m_Sit.y/100;
	}


	if( m_sPlayer == 0 )
	{
		id = enCollisionID_A_Defence;
	}
	else
	{
		id = enCollisionID_B_Defence;
	}

	if( m_sDir == DIR_RIGHT )
	{
		sx = m_Pos.x/100 + u;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	else
	{
		sx = m_Pos.x/100 - u - w;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	m_Kurai.On();
	m_Kurai.Set( id , sx , sy , sw , sh );


#ifdef _DEBUG
	CDraw::PutSprite( sx*100, sy*100 , 255 , enTexMain,0,0,sw,sh,0,0 ,ATR_DFLT , 0x8000ff00 );
#endif

}


void CSikakuPlayer::setAtari( Sint32 u , Sint32 v , Sint32 w, Sint32 h ,Sint32 type , Sint32 sDir )
{
	EnCollisionID id;
	Sint32 sx = m_Pos.x/100;
	Sint32 sy = m_Pos.y/100;
	Sint32 sw=0,sh=0;
	m_Kurai.m_sHp = 100;

	m_Atari.m_sAtEff = type;
	if( m_bAtackHit )
	{
		m_Atari.Off();
		return;
	}
	if( sDir == 0 )
	{
		m_Atari.m_sAtDir = m_sDir;
	}

	if( m_sPlayer == 0 )
	{
		id = enCollisionID_A_Atack;
	}
	else
	{
		id = enCollisionID_B_Atack;
	}

	if( m_sDir == DIR_RIGHT )
	{
		sx = m_Pos.x/100 + u;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	else
	{
		sx = m_Pos.x/100 - u - w;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	m_Atari.On();
	m_Atari.Set( id , sx , sy , sw , sh );

}


void CSikakuPlayer::controlMove()
{
	Sint32 dr = 0;

	m_Add.x = 0;

	if( JoyPush()&JOY_L )
	{
		m_Add.x = -160;	
		if( m_sDir == DIR_RIGHT ) dr = -1;	else	dr = 1;
	}
	else if( JoyPush()&JOY_R )
	{
		m_Add.x = 160;		
		if( m_sDir == DIR_RIGHT ) dr = 1;	else	dr = -1;
	}

	if( dr == 1 )
	{
		m_fRotation += ( 10 - m_fRotation )/10.f;
	}
	else if( dr == -1 )
	{
		m_fRotation += ( -10 - m_fRotation )/10.f;
	}
	else
	{
		m_fRotation += ( 0 - m_fRotation )/10.f;
	}

	if( dr == 1*m_sDir )
	{
		m_Punch.x += ( 2800 - m_Punch.x)/10;
	}
	else if( dr == -1*m_sDir )
	{
		m_Punch.x += ( 0 - m_Punch.x)/10;
	}
	else
	{
		m_Punch.x += ( 1200 - m_Punch.x)/10;
	}

	if( JoyPush()&JOY_U )
	{
		m_sJumpCnt = 0;
		m_bJump = gxTrue;
		CSub::PlayAudio( enAudioBoost );
		changeStat( enStatAir );
	}

	if( JoyPush()&JOY_D )
	{
		m_Add.x = 0;
		m_bCrouch = gxTrue;
	}

	if( m_pEnemy->GetPos()->x < m_Pos.x )
	{
		m_sDir = DIR_LEFT;
	}
	else
	{
		m_sDir = DIR_RIGHT;
	}

	m_Pos.x += m_Add.x;

	if( hissatsuCheck() )
	{
		if( m_sReversalCnt < 2 )
		{
			m_bReversal = gxTrue;
		}

	}
	else if( m_pCmd->Check( uCmdDash , sizeof(uCmdDash)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		m_sAnimCnt = 0;
		m_pCmd->ResetLog();
		changeStat( enStatFrontDash );
	}
	else if( m_pCmd->Check( uCmdBack , sizeof(uCmdBack)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		m_sAnimCnt = 0;
		m_pCmd->ResetLog();
		changeStat( enStatBackDash );
	}
	else
	{
		m_fHanaRot += (0 - m_fHanaRot)/10.f;

		if( m_bCrouch )
		{
			m_sAnimCnt = 0;

			if( JoyTrig()&BTN_A )
			{
				changeStat( enStatAsibarai );
			}
		}
		else
		{
			m_sAnimCnt = 0;

			if( JoyTrig()&BTN_A )
			{
				changeStat( enStatKick );
//				changeStat( enStatStan );
			}
		}
	}

	if( !m_bCrouch )
	{
		m_Sit.y -= 3;
		m_Sit.y += ( 0 - m_Sit.y )/5;
		if( m_Sit.y < 0 )
		{
			m_Sit.y = 0;
		}
		m_Punch.y += ((-1600 + gxUtil::Cos( (gxLib::GetGameCounter()*16)%360 )*400)-m_Punch.y)/10;
	}
	else
	{
		m_Sit.y += 3;
		m_Sit.y += ( 100 - m_Sit.y )/5;
		if( m_Sit.y > 100 )
		{
			m_Sit.y = 100;
		}
		m_Punch.y += (( -1200 + gxUtil::Cos( (gxLib::GetGameCounter()*16)%360 )*400)-m_Punch.y)/10;
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	setKurai( 0,0,0,0);//-16,-48+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

}


void CSikakuPlayer::controlJump()
{
	//--------------------------------------------
	//ジャンプ中のコントロール
	//--------------------------------------------

	if( m_bFirstAction )
	{
		m_sJumpDir = 0;
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
	}

	if( m_sJumpCnt < 4 )
	{
		m_bJumpKick = gxFalse;

		m_Sit.y = 20+m_sJumpCnt*5;

		m_Add.x = 0;
		m_Add.y = 0;

		if( JoyPush()&JOY_L )
		{
			m_sJumpDir = DIR_LEFT;
		}
		else if( JoyPush()&JOY_R )
		{
			m_sJumpDir = DIR_RIGHT;
		}

	}
	else if( m_sJumpCnt == 4 )
	{
		m_bJump = gxTrue;
		m_Add.y = 0;

		if( m_sJumpDir == DIR_LEFT )
		{
			m_Add.x = -340;
		}
		else if( m_sJumpDir == DIR_RIGHT )
		{
			m_Add.x = +340;
		}

		m_Add.y = -900;

	}
	else
	{
		if( m_sJumpCnt == 35 )
		{
			//壁ジャンプ

			if( IsKabegiwa() )
			{
				if( m_Pos.x <= m_pEnemy->GetPos()->x && JoyPush()&JOY_R )
				{
					m_Add.x = +340;
					m_Add.y = -900;
					new CEffHitmark( m_Pos.x , m_Pos.y );
					m_fRotation = 0.f;
				}
				else if( m_Pos.x >= m_pEnemy->GetPos()->x && JoyPush()&JOY_L )
				{
					m_Add.x = -340;
					m_Add.y = -900;
					new CEffHitmark( m_Pos.x , m_Pos.y );
					m_fRotation = 0.f;
				}
			}
		}

		if( m_Add.y < 800 ) m_Add.y += 40;

		if( !m_bJumpKick )
		{
			if( m_pCmd->Check( uCmdSenpukyaku , sizeof(uCmdSenpukyaku)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
			{
				changeStat( enStatKuchuTatsumaki );
			}
			else if( JoyPush()&BTN_A )
			{
				m_sAnimCnt = 0;
				m_bJumpKick = gxTrue;
				m_sAtackType = 0;
				m_sAtackType = 1;
				m_fRotation = 0.f;
				CSub::PlayAudio( enAudioKnouckle );
			}
			else if( JoyPush()&BTN_B )
			{
//				m_sAnimCnt = 0;
//				m_bJumpKick = gxTrue;
//				m_sAtackType = 1;
			}

		}
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			controlReset();

			m_sJumpCnt = 0;
			m_Add.x = 0;
			m_Add.y = 0;
			m_bJump = gxFalse;
			m_fRotation = 0.f;

			changeStat( enStatLanding );
		}

		m_bJumpKick = gxFalse;
	}
	else
	{
		if( m_bJumpKick )
		{
			if( m_sAtackType == 0 )
			{
				controlJumpKickYoko();
			}
			else
			{
				controlJumpKick();
			}
		}
	}

	//setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai( 0,0,0,0);

	if( m_bJumpKick )
	{
		m_fRotation += ( 0 - m_fRotation )/10.f;
	}
	else
	{
		if( m_sAnimCnt > 20 )
		{
			if( m_sJumpDir == 0 )
			{
				m_fRotation += ( 0 - m_fRotation )/20.f;
			}
			else if( m_sJumpDir != m_sDir )
			{
				m_fRotation += ( -460 - m_fRotation )/20.f;
			}
			else
			{
				m_fRotation += ( 460 - m_fRotation )/20.f;
			}
		}
	}

	m_sJumpCnt ++;
}


void CSikakuPlayer::controlLanding()
{
	if( m_bFirstAction )
	{
		m_sJumpCnt = 0;
		m_bFirstAction = gxFalse;
		CSub::PlayAudio( enAudioLanding );
	}
	m_Pos.y = _COD( UNDERLINE );

	m_Sit.y = 50+m_sJumpCnt*10;

	if( m_sJumpCnt > 4 )
	{
		changeStat( enStatGround );
	}

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai( 0,0,0,0);

	m_fRotation += ( 0 - m_fRotation )/10.f;

	m_sJumpCnt ++;

}

void CSikakuPlayer::controlFrontDash()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_Add.x = 1200;
		CSub::PlayAudio( enAudioDash );
	}
	else
	{
		m_Add.x --;
		m_Add.x += ( 0 - m_Add.x )/10;
		if( m_Add.x <= 0 )
		{
			m_Add.x = 0;
		}
		if( m_sAnimCnt >= 16 )
		{
			m_Add.x = 0;
			changeStat( enStatGround );
		}
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x*m_sDir;

	setKurai( 0,0,0,0);

	m_fRotation += ( 30 - m_fRotation )/10.f;

	if( m_sAnimCnt < 8 )
	{
		for(Sint32 ii=0;ii<2;ii++)
		{
			new CEffAtack( m_Pos.x , m_Pos.y - (gxLib::Rand()%48)*100 );
		}
	}

}

void CSikakuPlayer::controlBackDash()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_Add.x = -1200;
		CSub::PlayAudio( enAudioDash );
	}
	else
	{
		m_Add.x ++;
		m_Add.x += ( 0 - m_Add.x )/10;
		if( m_Add.x >= 0 )
		{
			m_Add.x = 0;
		}
		if( m_sAnimCnt >= 16 )
		{
			m_Add.x = 0;
			changeStat( enStatGround );
		}
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x*m_sDir;

	if( m_sAnimCnt > 12 )
	{
		setKurai( 0,0,0,0);
	}

	if( m_sAnimCnt < 8 )
	{
		for(Sint32 ii=0;ii<2;ii++)
		{
			new CEffAtack( m_Pos.x , m_Pos.y - (gxLib::Rand()%48)*100 );
		}
	}

	m_fRotation += ( -30 - m_fRotation )/10.f;

}


void CSikakuPlayer::controlWinner()
{
	//Winner!

	Float32 maxFrm = 50;

	if( m_bFirstAction )
	{
		m_sAnimCnt = 0;
		m_bFirstAction = gxFalse;
		m_sAtackWait = 0;
		m_bAtackHit = gxFalse;
		m_Add.x = 0;
		m_bJump = gxTrue;
		m_Add.y = -700;
	}

	if( m_Add.y < 800 ) m_Add.y += 30;


	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	m_Punch.x = -16*100;
	m_Punch.y = -3200 + 1600 * gxUtil::Cos(360*m_sAnimCnt/maxFrm);

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			controlReset();

			m_sJumpCnt = 0;
			m_Add.x = 0;
			m_Add.y = 0;
			m_bJump = gxFalse;

			m_bFirstAction = gxTrue;
		}
	}

	m_sJumpCnt ++;

	m_fRotation += ( -10 - m_fRotation )/10.f;

}


void CSikakuPlayer::controlStandKick()
{
	Float32 maxFrm = 24;

	if( m_bFirstAction )
	{
		CSub::PlayAudio( enAudioKnouckle );
		m_sAtackWait = 0;
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;

		if( ABS( m_Pos.x - m_pEnemy->GetPos()->x ) < enNageMaai*100 )
		{
			if( m_pEnemy->GetPos()->y >= _COD( UNDERLINE-16 ) )
			{
				if( ( m_sDir == DIR_RIGHT && JoyPush()&JOY_R) || ( m_sDir == DIR_LEFT && JoyPush()&JOY_L) )
				{
					if( m_pEnemy->IsNageable() )
					{
						m_pEnemy->SetThrow();
						changeStat( enStatThrowAtari );
						return;
					}
					else
					{
						changeStat( enStatThrowSukari );
						return;
					}
				}
			}
		}

	}

	//setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai(0,0,0,0);

	if( m_bAtackHit && hissatsuCheck() )
	{

	}
	else if( m_sAnimCnt > maxFrm )
	{
		m_sAtackWait ++;
		if( m_sAtackWait > 6 )
		{
			changeStat( enStatGround );
		}
		return;
	}

	m_Pos.y = _COD( UNDERLINE );
	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*200*m_sDir;

	m_Punch.x = -24*100;
	m_Punch.x += 96* gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*100;

	m_Punch.y  = (-32+(8* gxUtil::Cos( 360*m_sAnimCnt/maxFrm )))*100;


	if( m_sAnimCnt < maxFrm/2 )
	{
		setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 );
	}

	m_fRotation += ( 15+30* gxUtil::Sin( 360*m_sAnimCnt/maxFrm ) - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );
}


void CSikakuPlayer::controlJumpKickYoko()
{
	Float32 maxFrm = 128;

	if( m_sAnimCnt > maxFrm )
	{
		return;
	}

	Sint32 n = 180*m_sAnimCnt/16;
	if( n > 180 ) n = 180;

	m_Punch.x =  16*100;
	m_Punch.x += 32* gxUtil::Sin( n )*100;

	m_Punch.y = -24*100 + 1*m_Punch.x/3;

	setAtari( m_Punch.x/100-32, m_Punch.y/100-8 , 48,32 );

	m_fRotation += ( 10 - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );
}


void CSikakuPlayer::controlJumpKick()
{
	Float32 maxFrm = 28;

	if( m_sAnimCnt == 0 )
	{
		if( ( m_sDir == DIR_RIGHT && JoyPush()&JOY_R) || ( m_sDir == DIR_LEFT && JoyPush()&JOY_L) )
		{
			if( ABS( m_Pos.x - m_pEnemy->GetPos()->x ) < 32*100 )
			{
				if( ABS( m_Pos.y - m_pEnemy->GetPos()->y ) < 64*100 )
				{
					if( m_pEnemy->GetPos()->y <= _COD( UNDERLINE-64 ) )
					{
						if( m_pEnemy->IsNageable() )
						{
							m_pEnemy->SetThrow( enStatThrowKuchuKurai );
							//changeStat( enStatThrowAtari );
							changeStat( enStatThrowKuchuAtari );
						}
					}
				}
			}
		}
	}

	if( m_sAnimCnt > maxFrm )
	{
		return;
	}


	m_Punch.x = -16*100;
	m_Punch.x += 80* gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*100;

	m_Punch.y = 0*100 + 1*m_Punch.x/3;

	setAtari( m_Punch.x/100-48, m_Punch.y/100-8 , 64,48 );

	m_fRotation += ( -50 - m_fRotation )/10.f;

	if( m_sAnimCnt < 15 )
	{
		atackEffect( m_Hand.x , m_Hand.y );
	}
}


void CSikakuPlayer::controlAsibarai()
{
	Float32 maxFrm = 20;

	m_bCrouch = gxTrue;

	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
		CSub::PlayAudio( enAudioKnouckle );
	}

	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*-100*m_sDir;
	m_Pos.y = _COD( UNDERLINE );

	m_Punch.x = 16*100*m_sDir;
	m_Punch.x += 84* gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*100;
	m_Punch.y  = -2*100+ gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*1600;

	if( m_bAtackHit && hissatsuCheck() )
	{

	}
	else if( m_sAnimCnt > maxFrm )
	{
		changeStat( enStatGround );
	}

	if( m_sAnimCnt < maxFrm/2 )
	{
		setAtari( m_Punch.x/100-24, m_Punch.y/100-8 , 32,16 );
	}

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai(0,0,0,0);

	m_fRotation += ( -20-30* gxUtil::Sin( 360*m_sAnimCnt/maxFrm ) - m_fRotation )/10.f;
	if( m_sAnimCnt < 13 )
	{
		atackEffect( m_Hand.x , m_Hand.y );
	}
}


void CSikakuPlayer::controlStandPuch()
{
	Float32 maxFrm = 8;

	if( m_bFirstAction )
	{
		m_sAtackWait = 0;
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
		CSub::PlayAudio( enAudioKnouckle );
	}

	setKurai(0,0,0,0);

	if( m_bAtackHit && hissatsuCheck() )
	{

	}
	else if( m_sAnimCnt > maxFrm )
	{
		//m_sAtackWait ++;
		//if( m_sAtackWait > 4 )
		{
			changeStat( enStatGround );
		}
		return;
	}

	m_Pos.y = _COD( UNDERLINE );
	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*200*m_sDir;

	m_Punch.x = 16*100;
	m_Punch.x += 48* gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*100;

	m_Punch.y  = -24*100;


	if( m_sAnimCnt < maxFrm/2 )
	{
		setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 );
	}

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

	m_fRotation += ( 20 - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );

}

void CSikakuPlayer::controlLowPunch()
{
	Float32 maxFrm = 8;

	m_bCrouch = gxTrue;

	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
	}

	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*-100*m_sDir;
	m_Pos.y = _COD( UNDERLINE );

	m_Punch.x = 24*100;
	m_Punch.x += 40* gxUtil::Sin( 180*m_sAnimCnt/maxFrm )*100;
	m_Punch.y  = 0*100;

	if( m_bAtackHit && hissatsuCheck() )
	{

	}
	else if( m_sAnimCnt > maxFrm )
	{
		changeStat( enStatGround );
	}

	if( m_sAnimCnt < maxFrm/2 )
	{
		setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 );
	}

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai(0,0,0,0);

	m_fRotation += ( 10 - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );
}


void CSikakuPlayer::controlThrowSukari()
{
	Float32 maxFrm = 32;

	m_bCrouch = gxTrue;

	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
		if( ABS( m_Pos.x - m_pEnemy->GetPos()->x ) < 48*100 )
		{
			if( m_pEnemy->GetPos()->y >= _COD( UNDERLINE-16 ) )
			{
				if( m_pEnemy->IsNageable() )
				{
					m_pEnemy->SetThrow();
					changeStat( enStatThrowAtari );
				}
			}
		}
	}

	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*160*m_sDir;
	m_Pos.y = _COD( UNDERLINE );

	m_Punch.x  = -16*100;
	m_Punch.x += 48* gxUtil::Sin( 90*m_sAnimCnt/maxFrm )*100;
	m_Punch.y  = -24*100;

	if( m_sAnimCnt > maxFrm )
	{
		changeStat( enStatGround );
	}

	setKurai(0,0,0,0);

	m_fRotation += ( 50 - m_fRotation )/10.f;

}

void CSikakuPlayer::controlThrowAtari()
{
	Float32 maxFrm = 50;

	if( m_bFirstAction )
	{
		m_sAnimCnt = 0;
		m_bFirstAction = gxFalse;
		m_sAtackWait = 0;
		m_bAtackHit = gxFalse;
		m_Add.x = 0;
		m_bJump = gxTrue;
		m_Add.y = -300;
	}

	if( m_Add.y < 800 ) m_Add.y += 30;


	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	m_Punch.x = -16*100;
	m_Punch.y = -3200 + 1600 * gxUtil::Cos(360*m_sAnimCnt/maxFrm);

	if( m_Pos.y >= _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		m_fRotation += ( 0 - m_fRotation )/10.f;

		if( 1 )//m_bJump )
		{
			m_bJump = gxFalse;
		}

		if( m_sJumpCnt >= 48 )
		{
			m_Add.x = 0;
			m_Add.y = 0;
			controlReset();
			m_sJumpCnt = 0;
			m_bFirstAction = gxTrue;
			changeStat( enStatGround );
		}
	}
	else
	{
		m_fRotation += ( -60 - m_fRotation )/10.f;
	}

	m_sJumpCnt ++;

}


void CSikakuPlayer::controlThrowKurai()
{
	if( m_bFirstAction )
	{
		CSub::PlayAudio( enAudioKnouckle );

		m_bFirstAction = gxFalse;
		m_sJumpCnt = 0;
		m_bJump = gxTrue;
		m_sStanWait = 0;

//		m_Pos.y = _COD( UNDERLINE );
		m_Pos.y = m_pEnemy->GetPos()->y;

		if( m_pEnemy->GetPos()->x < GetPos()->x )
		{
			m_Pos.x = m_pEnemy->GetPos()->x + 3200;
			m_Add.x = -900;
			m_sDir = DIR_LEFT;
		}
		else
		{
			m_Pos.x = m_pEnemy->GetPos()->x - 3200;
			m_Add.x = 900;
			m_sDir = DIR_RIGHT;
		}

		m_Add.y = -650;
	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Add.x += -m_Add.x / 100;

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );
		controlReset();
		m_sJumpCnt = 0;
		//m_Add.x = 0;
		m_Add.y = 0;
		m_bJump = gxFalse;
		//changeStat( enStatGround );
		m_sHP -= 120;

		m_fRotation = -90;

		new CEffHitmark( m_Pos.x , m_Pos.y );
		CSikakuGameMain::GetInstance()->Quake( 30 , 3 );

		if( IsDead() )
		{
			changeStat( enStatDamageDead );
		}
		else
		{
			changeStat( enStatDamageDown );
		}
	}

	m_sJumpCnt ++;

	if( m_sJumpCnt < 20 )
	{
		m_fRotation += ( 90 - m_fRotation )/10.f;
	}
	else
	{
		m_fRotation += ( 270 - m_fRotation )/10.f;
	}

	m_fHanaRot += ( -90 - m_fHanaRot )/30.f;

}


void CSikakuPlayer::controlThrowKuchuAtari()
{
	Float32 maxFrm = 50;

	if( m_bFirstAction )
	{
		m_sAnimCnt = 0;
		m_bFirstAction = gxFalse;
		m_sAtackWait = 0;
		m_bAtackHit = gxFalse;
		m_Add.x = 0;
		m_bJump = gxTrue;
		m_Add.y = -300;
	}

	if( m_Add.y < 800 ) m_Add.y += 30;


	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	m_Punch.x = -16*100;
	m_Punch.y = -3200 + 1600 * gxUtil::Cos(360*m_sAnimCnt/maxFrm);

	if( m_Pos.y >= _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		m_fRotation += 360;
		m_fRotation += ( 0 - m_fRotation )/10.f;

		if( 1 )//m_bJump )
		{
			m_bJump = gxFalse;
		}

		if( m_sJumpCnt >= 48 )
		{
			m_Add.x = 0;
			m_Add.y = 0;
			controlReset();
			m_sJumpCnt = 0;
			m_bFirstAction = gxTrue;
			changeStat( enStatLanding );
		}
	}
	else
	{
		m_fRotation += ( -(60+240) - m_fRotation )/10.f;
	}

	m_sJumpCnt ++;

}


void CSikakuPlayer::controlThrowKuchuKurai()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_sJumpCnt = 0;
		m_bJump = gxTrue;
		m_sStanWait = 0;

//		m_Pos.y = _COD( UNDERLINE );
		m_Pos.y = m_pEnemy->GetPos()->y;

		if( m_pEnemy->GetPos()->x < GetPos()->x )
		{
			m_Pos.x = m_pEnemy->GetPos()->x + 3200;
			m_Add.x = -900;
			m_sDir = DIR_LEFT;
		}
		else
		{
			m_Pos.x = m_pEnemy->GetPos()->x - 3200;
			m_Add.x = 900;
			m_sDir = DIR_RIGHT;
		}

		m_Add.y = -650;
	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Add.x += -m_Add.x / 100;

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );
		controlReset();
		m_sJumpCnt = 0;
		//m_Add.x = 0;
		m_Add.y = 0;
		m_bJump = gxFalse;
		//changeStat( enStatGround );
		m_sHP -= 120;

		m_fRotation = -90;

		new CEffHitmark( m_Pos.x , m_Pos.y );
		CSikakuGameMain::GetInstance()->Quake( 30 , 3 );

		if( IsDead() )
		{
			changeStat( enStatDamageDead );
		}
		else
		{
			changeStat( enStatDamageDown );
		}
	}

	m_sJumpCnt ++;

	if( m_sJumpCnt < 20 )
	{
		m_fRotation += ( 90 - m_fRotation )/10.f;
	}
	else
	{
		m_fRotation += ( 270 - m_fRotation )/10.f;
	}

	m_fHanaRot += ( -90 - m_fHanaRot )/30.f;

}


void CSikakuPlayer::controlSyouryuken()
{
	Float32 maxFrm = 50;

	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_sAtackWait = 0;
		m_bAtackHit = gxFalse;
		m_fRotation = 30;
		CSub::PlayAudio( enAudioBoost );
	}
	else
	{
		if( m_sAnimCnt < 3 )
		{
			m_Add.x = 900*m_sDir;
		}
		else if( m_sAnimCnt == 3 )
		{
			m_Add.x = 550*m_sDir;
			m_bJump = gxTrue;
			m_Add.y = -650;
		}
		else
		{
			if( m_Add.y < 800 ) m_Add.y += 20;
		}
	}

	m_Add.x += ( 0 - m_Add.x)/30;

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	m_Punch.x = 16*100;
	m_Punch.x = gxUtil::Sin( 240*m_sAnimCnt/maxFrm )*3200;
	m_Punch.y = -4800+ gxUtil::Cos( -45+360*m_sAnimCnt/maxFrm )*4800;

	if( m_bAtackHit )
	{
		if( m_sAnimCnt < 20 )
		{
			if( m_sAtackWait > 3 )
			{
//				m_bAtackHit = gxFalse;
			}
		}

		m_sAtackWait ++;
	}

	if( m_Pos.y >= _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			m_Add.x = 0;
			m_Add.y = 0;
			m_bJump = gxFalse;
			controlReset();
			m_sJumpCnt = 0;
			changeStat( enStatLanding );
			return;
		}

		m_bJumpKick = gxFalse;
	}

	m_sDirPos += ( 0 - m_sDirPos ) /5;

	if( m_sAnimCnt < 32 )
	{
		if( m_sAtackWait )
		{
			setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 ,1);
		}
		else
		{
			setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 ,1);
		}
	}

	if( m_sAnimCnt > 18 )//30 )
	{
		setKurai( 0,0,0,0 );//-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	}

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_sJumpCnt ++;

	m_fRotation += ( -30 - m_fRotation )/30.f;

	atackEffect( m_Hand.x , m_Hand.y );

}


void CSikakuPlayer::controlHadouken()
{
	Float32 maxFrm = 64;

	if( m_bFirstAction )
	{
		m_bCrouch = 0;
		m_sAtackWait = 0;
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
	}

	setKurai(0,0,0,0);

	if( m_sAnimCnt > 20 )
	{
		if( m_sAtackWait <16 )
		{
			m_sAtackWait ++;
			return;
		}
	}

	if( m_sAnimCnt == 16 )
	{
		new CSikakuEffect( m_Pos.x+3200*m_sDir , m_Pos.y -24*100, m_sDir , m_sPlayer );
		CSub::PlayAudio( enAudioBazooka );
	}

	if( m_sAnimCnt > maxFrm )
	{
		m_sAtackWait ++;
		if( m_sAtackWait > 4 )
		{
			changeStat( enStatGround );
		}
		return;
	}

	m_Pos.y = _COD( UNDERLINE );
	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*100*m_sDir;

	m_Punch.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*200;
	m_Punch.y  = -24*100;

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_fRotation += ( 30 - m_fRotation )/10.f;

}


void CSikakuPlayer::controlSenpukyaku()
{

	Float32 maxFrm = 96;

	m_bCrouch = gxTrue;

	if( m_bFirstAction )
	{
		m_bAtackHit = gxFalse;
		m_Add.y = 0;
		m_bFirstAction = gxFalse;
		m_bJump = gxTrue;
		CSub::PlayAudio( enAudioBoost );
	}

	m_Pos.x += 200*m_sDir;

	m_Punch.x  = 16*100*m_sDir;
	m_Punch.x += 56* gxUtil::Sin( 180+m_sAnimCnt*12 )*100;
	m_Punch.y  = 8*100;

	if( m_sAnimCnt < 160 )
	{
		if( m_sAnimCnt%32 == 16 ) CSub::PlayAudio( enAudioJump );
	}

	if( m_sAnimCnt <32 )
	{
		m_Pos.y += (_COD( UNDERLINE -48 ) - m_Pos.y)/10;
	}
	if( m_sAnimCnt > maxFrm )
	{
		m_bJump = gxTrue;
		m_Pos.y += m_Add.y;
		if( jumpNow() )
		{
			changeStat( enStatLanding );
		}
	}
	else
	{
		if( m_sAtackWait == 0 && m_bAtackHit )
		{
			m_bAtackHit = gxFalse;
			m_sAtackWait = 16;
		}
		else if( m_sAtackWait > 0 )
		{
			m_sAtackWait --;
		}
		else
		{
			setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 ,1);
		}
	}

	setKurai(0,0,0,0);//-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

	m_fRotation += ( 0 - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );

	m_fHanaRot += ( 0 - m_fHanaRot )/10.f;

}

void CSikakuPlayer::controlKuchuTatsumaki()
{

	Float32 maxFrm = 96;

	m_bCrouch = gxTrue;

	if( m_bFirstAction )
	{
		m_Add.y = -4*100;
		m_bJump = gxTrue;
		m_bAtackHit = gxFalse;
		m_bFirstAction = gxFalse;
	}

	if( m_sAnimCnt < 160 )
	{
		if( m_sAnimCnt%16 == 15 ) CSub::PlayAudio( enAudioBoost );
	}

	m_Punch.x = 16*100*m_sDir;
	m_Punch.x += 56* gxUtil::Sin( 100+m_sAnimCnt*16 )*100;
	m_Punch.y  = 0*100;

	m_Pos.x += m_Add.x;
	m_Pos.y += m_Add.y;

	if( jumpNow() )
	{
		
	}
	else
	{
	}

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	setAtari( m_Punch.x/100-8, m_Punch.y/100-8 , 16,16 );
	setKurai( 0,0,0,0);//-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

	m_fRotation += ( 0 - m_fRotation )/10.f;

	atackEffect( m_Hand.x , m_Hand.y );

}


void CSikakuPlayer::controlSinkuHadouken()
{
	Float32 maxFrm = 64;

	if( m_bFirstAction )
	{
		CSub::PlayAudio( enAudioPurge );
		m_sAtackWait = 0;
		m_bFirstAction = gxFalse;
		m_bAtackHit = gxFalse;
	}

	if( m_sAnimCnt < 10 )
	{
		new CEffBeam( m_Pos.x+m_Punch.x*m_sDir , m_Pos.y + m_Punch.y );
	}

	if( m_sAnimCnt == 10 )
	{
		CSikakuGameMain::GetInstance()->TheWorld( 60 );
		m_sCB = 0;
	}

	if( m_sAnimCnt > 48 )
	{
		if( m_sAtackWait <16 )
		{
			m_sAtackWait ++;
			return;
		}
	}

	CSikakuEffect *p;
	switch( m_sAnimCnt ){
	case 12+4*0:
	case 12+4*1:
	case 12+4*2:
	case 12+4*3:
		p = new CSikakuEffect( m_Pos.x+2400*m_sDir , m_Pos.y -16*100, m_sDir , m_sPlayer );
		CSub::PlayAudio( enAudioBazooka );
		p->SetDir( m_sDir );
		break;

	case 12+4*4:
		p = new CSikakuEffect( m_Pos.x+2400*m_sDir , m_Pos.y -16*100, m_sDir , m_sPlayer , gxTrue );
		CSub::PlayAudio( enAudioBazooka );
		p->SetDir( m_sDir );
		break;
	}

	if( m_sAnimCnt > maxFrm )
	{
		m_sAtackWait ++;
		if( m_sAtackWait > 4 )
		{
			changeStat( enStatGround );
		}
		return;
	}

	m_Pos.y = _COD( UNDERLINE );
	m_Pos.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*100*m_sDir;

	if( m_sAnimCnt < 10 )
	{
		m_Punch.x += ( -32*100 - m_Punch.x )/10;
		m_Punch.y += (  16*100 - m_Punch.y )/10;
	}
	else
	{
		m_Punch.x += gxUtil::Sin( 360*m_sAnimCnt/maxFrm )*200;
		m_Punch.y  = -16*100;
	}

//	setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai( 0,0,0,0 );

	m_fRotation += ( 20 - m_fRotation )/10.f;

}

void CSikakuPlayer::controlDamageGround()
{
	if( m_bFirstAction )
	{
		controlReset();
		m_bJump = gxFalse;
		//m_Add.x *= 800;
		m_Add.y = 0;
		m_Pos.y = _COD( UNDERLINE );
		m_bFirstAction = gxFalse;
	}
	else
	{
//		m_Add.x ++;
		m_Add.x += ( 0 - m_Add.x )/5;
//		if( m_Add.x >= 0 )
//		{
//			m_Add.x = 0;
//		}
		if( m_sAnimCnt >= 28 )
		{
			m_Add.x = 0;
			changeStat( enStatGround );
		}
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x*m_sDir;

	//setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	setKurai(0,0,0,0);

	m_fRotation += ( -30 - m_fRotation )/10.f;

	m_fHanaRot += ( -60 - m_fHanaRot )/10.f;

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_sReversalCnt = 0;
}

void CSikakuPlayer::controlDamageAir()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;

		m_bJump = gxTrue;
		m_Add.y = 0;

		if( m_Add.x < 0 )
		{
			m_Add.x = -240;
		}
		else
		{
			m_Add.x = +240;
		}

		m_Add.y = -200;

	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			controlReset();

			m_sJumpCnt = 0;
			m_Add.y = 0;
			changeStat( enStatDamageDown );
			new CEffHitmark( m_Pos.x , m_Pos.y );
			m_bJump = gxFalse;
		}

		m_bJumpKick = gxFalse;
	}

	//setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );

	m_sJumpCnt ++;

	m_fRotation += ( -90 - m_fRotation )/10.f;

	m_fHanaRot += ( -60 - m_fHanaRot )/10.f;
}


void CSikakuPlayer::controlDamageFuttobi()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_sJumpCnt = 0;

		m_bJump = gxTrue;

		if( m_Add.x < 0 )
		{
			m_Add.x = -320;
		}
		else
		{
			m_Add.x = +320;
		}

		m_Add.y = -500;

	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			new CEffHitmark( m_Pos.x , m_Pos.y );

			controlReset();

			m_sJumpCnt = 0;
			//m_Add.x = 0;
			m_Add.y = 0;
			m_bJump = gxFalse;
			changeStat( enStatDamageDown );
		}

		m_bJumpKick = gxFalse;
	}

	if( m_sJumpCnt < 8 )
	{
		//setKurai(-16,-32+ 16*m_Sit.y/100,32, 64- 16*m_Sit.y/100 );
	}

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_sJumpCnt ++;

	m_fRotation += ( -90 - m_fRotation )/10.f;

	m_fHanaRot += ( -60 - m_fHanaRot )/10.f;
}

void CSikakuPlayer::controlDamageDown()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_sJumpCnt = 0;

		m_sAnimCnt = 0;
		m_bJump = gxTrue;

		if( m_Add.x < 0 )
		{
			m_Add.x = -320;
		}
		else
		{
			m_Add.x = +320;
		}

		m_Add.y = -200;

		CSub::PlayAudio( enAudioLanding );
	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	m_Add.x += -m_Add.x/10;

	if( m_Pos.y >= _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( 1 )//m_bJump )
		{
			m_bJump = gxFalse;
		}
		if( m_sJumpCnt >= 30 )
		{
			m_Add.x = 0;
			m_Add.y = 0;
			m_sJumpCnt = 0;
			controlReset();
			changeStat( enStatGetUp );
		}

		m_bJumpKick = gxFalse;
	}

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_sJumpCnt ++;

	m_fRotation += ( -90 - m_fRotation )/3.f;

	m_fHanaRot += ( -90 - m_fHanaRot )/10.f;
}


void CSikakuPlayer::controlGetup()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_fRotation = -90;//+360;
		controlReset();
		CSub::PlayAudio( enAudioJump );
	}

	m_Add.x = 0;
	m_Add.y = 0;
	m_Pos.y = _COD( UNDERLINE );

	if( m_sAnimCnt < 10 )
	{
	}
	if( m_sAnimCnt < 20 )
	{
		m_fRotation += ( 0 - m_fRotation )/10.f;
	}
	else
	{
		changeStat( enStatGround );

		if( m_sStanWait > 0 )
		{
			changeStat( enStatStan );
		}
	}

	m_fHanaRot += ( 0 - m_fHanaRot )/10.f;

	m_sReversalCnt = 0;
}


void CSikakuPlayer::controlStan()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_fRotation = 0;
		controlReset();
		m_fHanaRot = 0;

		CSub::PlayAudio( enAudioDash );
	}

	m_Add.x = 0;
	m_Add.y = 0;
	m_Pos.y = _COD( UNDERLINE );

	m_fRotation += ( gxUtil::Cos( (m_sAnimCnt*4)%360 )*16 - m_fRotation )/10.f;
	m_fHanaRot  += ( gxUtil::Cos( (m_sAnimCnt*4)%360 )*32 - m_fHanaRot  )/10.f;

	if( JoyTrig() )
	{
		m_sAnimCnt += 12;
	}

	if( m_sAnimCnt > 800 )
	{
		m_sStanWait = 0;
		changeStat( enStatGround );
	}

	m_sReversalCnt = 0;

	setKurai(0,0,0,0);

}


void CSikakuPlayer::controlDamageDead()
{
	if( m_bFirstAction )
	{
		m_bFirstAction = gxFalse;
		m_sJumpCnt = 0;
		m_bJump = gxTrue;

		if( m_pEnemy->GetPos()->x < GetPos()->x )
		{
			m_Add.x = 100;
		}
		else
		{
			m_Add.x = -100;
		}

		m_Add.y = -800;


		CSub::PlayAudio( enAudioCrash );
	}
	else
	{
		if( m_Add.y < 800 ) m_Add.y += 30;
	}

	m_Add.x += -m_Add.x / 100;

	m_Pos.y += m_Add.y;
	m_Pos.x += m_Add.x;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		if( m_Pos.y > _COD( UNDERLINE + 120 ) )
		{
			m_bExist = gxFalse;
			m_Add.x = 0;
			m_Pos.y = _COD( UNDERLINE + 120 );
		}
	}
	else
	{
		CSikakuGameMain::GetInstance()->SetSlow(30);
	}

	m_Sit.y -= 3;
	m_Sit.y += ( 0 - m_Sit.y )/5;
	if( m_Sit.y < 0 )
	{
		m_Sit.y = 0;
	}

	m_sJumpCnt ++;

	m_fRotation += ( -240 - m_fRotation )/30.f;

	m_fHanaRot += ( -0 - m_fHanaRot )/10.f;
}


gxBool CSikakuPlayer::jumpNow()
{
	if( m_Add.y < 800 ) m_Add.y += 30;

	if( m_Pos.y > _COD( UNDERLINE ) )
	{
		m_Pos.y = _COD( UNDERLINE );

		if( m_bJump )
		{
			controlReset();

			m_sJumpCnt = 0;
			m_Add.x = 0;
			m_Add.y = 0;
			m_bJump = gxFalse;
			changeStat( enStatLanding );
			return gxTrue;
		}

		m_bJumpKick = gxFalse;
	}

	m_sJumpCnt ++;

	return gxFalse;
}


void CSikakuPlayer::atackEffect( Sint32 x , Sint32 y )
{
	if( m_Atari.m_sFlag )
	{
		new CEffAtack(x,y);
	}
}


gxBool CSikakuPlayer::hissatsuCheck()
{
	if( m_pEnemy->IsDead() )
	{
		return gxFalse;
	}


	if( m_pCmd->Check( uCmdSinkuHadouken , sizeof(uCmdSinkuHadouken)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		if( IsCBMaximum() )
		{
			changeStat( enStatSinkuHadoken );
		}
		return gxTrue;
	}
	else if( m_pCmd->Check( uCmdSyouryuken , sizeof(uCmdSyouryuken)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		changeStat( enStatSyouryuken );
		return gxTrue;
	}
	else if( m_pCmd->Check( uCmdHadouken , sizeof(uCmdHadouken)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		if( !CSikakuGameMain::GetInstance()->IsTobidougu( m_sPlayer ) )
		{
			//画面内に２つは出ない
			changeStat( enStatHadouken );
		}
		return gxTrue;
	}
	else if( m_pCmd->Check( uCmdSenpukyaku , sizeof(uCmdSenpukyaku)/sizeof(Uint32) , ( m_sDir == DIR_LEFT )? gxTrue : gxFalse ) )
	{
		changeStat( enStatSenpukyaku );
		return gxTrue;
	}
	return gxFalse;
}

void CSikakuPlayer::atariJudge()
{
	//------------------------------------------------
	//当たり判定
	//------------------------------------------------

	if( m_Atari.IsHit() )
	{
		m_Atari.m_bHit = gxFalse;

		if( !m_bAtackHit )
		{
			new CEffHitmark( m_Pos.x + m_Punch.x*m_sDir , m_Pos.y + m_Punch.y  );
			m_bAtackHit = gxTrue;
			m_sHitStopFrm = enHitStopFrm;

			if( m_bReversal )
			{
				new CEffComboDisp( m_sPlayer , -2 );
				m_bReversal = gxFalse;
			}

			if( m_pEnemy->IsKabegiwa() )
			{
				if( m_pEnemy->GetPos()->x > m_Pos.x )
				{
					m_sKnockBack = -400;
				}
				else
				{
					m_sKnockBack = 400;
				}
			}
		}
	}

	if( m_Kurai.IsHit() )
	{
		gxBool bStan = gxFalse;

		m_sComboCnt ++;
		m_sHP -= 80;
		m_Kurai.m_bHit = gxFalse;

		m_pEnemy->AddCB( 80 );

		if( m_sStanWait > 0 )
		{
			m_sStanWait = 0;
		}
		else
		{
			m_sStanDamage += enStanDamage;
			if( m_sStanDamage > 100 )
			{
				bStan = gxTrue;
				m_sStanDamage = 0;
				m_sStanWait = 1;
			}
		}


		m_sDamageWait = enHitStopFrm;

		if( IsDead() )
		{
			m_sStanWait = 0;
			changeStat( enStatDamageDead );
		}
		else if( m_bJump && m_Pos.y < _COD( UNDERLINE-24 ) )
		{
			if( m_pEnemy->GetPos()->x < GetPos()->x )
			{
				m_Add.x = 100;
			}
			else
			{
				m_Add.x = -100;
			}

			CSub::PlayAudio( enAudioExplosion );
			changeStat( enStatDamageAir );
		}
		else
		{
			if( m_Kurai.m_sDmEff == 1 )
			{
				m_Add.y = -800;
				m_Add.x = m_Kurai.m_sDmDir*800;
				CSub::PlayAudio( enAudioExplosion );
				changeStat( enStatDamageFuttobi );
			}
			else
			{
				//通常ヒット

				//めくり対応
				if( m_pEnemy->m_sDir == DIR_RIGHT )
				{
					m_Add.x = 800*m_sDir;
				}
				else
				{
					m_Add.x = -800*m_sDir;
				}

				if( m_sStanDamage > 100 )
				{
//					CSub::PlayAudio( enAudioHit );
					CSub::PlayAudio( enAudioPurge );
					changeStat( enStatDamageFuttobi );
				}
				else
				{
					changeStat( enStatDamageGround );

					if( bStan )
					{
						if( m_pEnemy->GetPos()->x < GetPos()->x )
						{
							m_Add.x = 100;
						}
						else
						{
							m_Add.x = -100;
						}
						CSub::PlayAudio( enAudioPurge );
						changeStat( enStatDamageFuttobi );
					}
					else
					{
						CSub::PlayAudio( enAudioHit );
					}
				}
			}
		}
	}

	m_Atari.Off();
	m_Kurai.Off();

}


void CSikakuPlayer::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CSikakuPlayer::Draw()
{
	//------------------------------------------------
	//------------------------------------------------

	Sint32 ux,uy;
	Sint32 dx,dy;
	Uint32 argb = ARGB_DFLT;
	
	if( m_sPlayer == 1 ) argb = 0xffff0000;

	dx = m_Pos.x;
	dy = m_Pos.y;

	if( m_sDamageWait%4<2 )
	{
		dx = dx-2*100;
	}

//	if( gxLib::Joy(0)->psh&JOY_L ) m_fRotation --;
//	if( gxLib::Joy(0)->psh&JOY_R ) m_fRotation ++;


	Float32 fRot = m_fRotation;
	Sint32 atr = ATR_DFLT;
	Sint32 dst = 0;

	if( m_sDir == DIR_LEFT ) atr = ATR_FLIP_X;

	//ボディ
	CDraw::PutSprite(dx,dy,32,enTexMain,0,0,24,38,12,32 , atr , argb,fRot );

	//影
	Sint32 yy = _COD( UNDERLINE );
	if( m_Pos.y  > yy ) yy = m_Pos.y;
	CDraw::PutSprite(dx,yy,31,enTexMain,0,0,48,8,24,-4 , atr , 0xE0000080 );

	//顔
	fRot = m_fRotation - 90.f;
	dst = -m_Sit.y*16 + _COD(48);
	ux = dx + gxUtil::Cos( fRot )*dst*m_sDir;
	uy = dy + gxUtil::Sin( fRot )*dst;
	CDraw::PutSprite(ux,uy,32,enTexMain,0,0,16,16,8,8 , atr , argb,fRot);

	//はな
	fRot = m_fRotation - 90.f;
	dst = -m_Sit.y*16 + _COD(48);
	ux = gxUtil::Cos( fRot )*dst;
	uy = gxUtil::Sin( fRot )*dst;


	fRot = m_fRotation + m_fHanaRot;
	dst = m_sDirPos*16*m_sDir;
	ux += gxUtil::Cos( fRot )*dst;
	uy += gxUtil::Sin( fRot )*dst;

	CDraw::PutSprite( dx+ux*m_sDir , dy+uy, 33 , enTexMain,0,0,8,8,4,4 , atr , argb ,fRot);

	//パンチ
	
	Sint32 px,py;

	fRot = m_fRotation - 90.f;
	dst = 0;//_COD(32);
	ux = dx + gxUtil::Cos( fRot )*dst;
	uy = dy + gxUtil::Sin( fRot )*dst;

	px = (m_Punch.x*m_sDir);
	py = (m_Punch.y);
	CDraw::PutSprite( ux + px, uy +py , 32 , enTexMain,0,0,16,16,8,8 ,ATR_DFLT , argb );


	m_Hand.x = ux + px;
	m_Hand.y = uy + py;

	gxPos _pos = m_Pos;

	Sint32 wx = CSikakuGameMain::GetInstance()->GetWorldX();

	if( CSikakuGameMain::GetInstance()->IsOnline( ) )
	{
		if( CSikakuGameMain::GetInstance()->GetCurrent() == m_sPlayer )
		{
			gxLib::Printf( _DIV(_pos.x-wx) ,_DIV(_pos.y)-64,200 , ATR_DFLT , 0xffff0000 , "YOU" );
		}
	}

#ifdef _DEBUG
	//gxLib::Printf( _DIV(_pos.x-wx) ,_DIV(_pos.y)-96,200 , ATR_DFLT , 0xffff0000 , "%s" , m_Kurai.m_sFlag? " " : "Muteki" );
	gxLib::Printf( _DIV(_pos.x-wx) ,_DIV(_pos.y)-96,200 , ATR_DFLT , 0xffff0000 , "%d" , m_sStanDamage );
#endif

}


