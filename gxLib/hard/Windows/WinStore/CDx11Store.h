﻿#ifndef _CDIRECTX11_H_
#define _CDIRECTX11_H_

interface IDeviceNotify
{
	virtual void OnDeviceLost() = 0;
	virtual void OnDeviceRestored() = 0;
};

#if 0
// MVP マトリックスを頂点シェーダーに送信するために使用する定数バッファー。
struct ModelViewProjectionConstantBuffer
{
	DirectX::XMFLOAT4X4 model;
	DirectX::XMFLOAT4X4 view;
	DirectX::XMFLOAT4X4 projection;
};
#endif

// 頂点シェーダーへの頂点ごとのデータの送信に使用します。
struct VertexPositionColorTexCoord
{
	DirectX::XMFLOAT4 pos;
	DirectX::XMFLOAT4 color;
	DirectX::XMFLOAT2 texcoord;												// テクスチャ U, V

	//追加
	DirectX::XMFLOAT2 scale;
	DirectX::XMFLOAT2 offset;
	Float32    rotation;
	DirectX::XMFLOAT2 flip;
	DirectX::XMFLOAT4 blend;

};

typedef struct ConstantBufferForView3D
{
	Float32 pos[4];
} ConstantBufferForView3D;


struct TextureData {														// テクスチャテータ定義
	TextureData()
	{
		//texture2D = NULL;
		//shaderResourceView = NULL;
		//renderTargetView = NULL;
	}
	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2D;						// テクスチャ本体
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shaderResourceView;	// シェーダリソースビュー
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	 renderTargetView;		//レンダーターゲットビュー
};


// すべての DirectX デバイス リソースを制御します。
class CDirectX11
{
enum {
	enBasicBuff   = MAX_MASTERTEX_NUM + 0,	//ノンテクスチャポリゴン用
	enWallPaper,	//256x256の固定壁紙
	enGameScreen1,	//WindowW x WindowHのゲーム画面
	enGameScreen2,	//WindowW x WindowHのゲーム画面
	enBackBuff,		//最終スクリーン画面用フレームバッファ
	enCaptureBuff,	//キャプチャ専用バッファ
	enSpecialBuffNum = 6,
};

enum {
	//頂点バッファーと、インデックスバッファーのサイズ、１頂点36bytes => １三角 = 108bytes => 1MB = 9709ポリゴン
	enVertexBufferSize = 1024*1024*32,//(MB)
	enIndexBufferSize  = 1024*1024*8,//(MB)
	enTexturePageMax   = MAX_MASTERTEX_NUM + enSpecialBuffNum,
};

public:
	CDirectX11();
	~CDirectX11();
	//void SetWindow(Windows::UI::Core::CoreWindow^ window);

	void Reset();

	void SetLogicalSize(Windows::Foundation::Size logicalSize)
	{
		if( m_logicalSize != logicalSize )
		{
			//reset();
		}
		m_logicalSize = logicalSize;
	}

	static CDirectX11* GetInstance()
	{
		if( s_pInsatnce == NULL )
		{
			s_pInsatnce = new CDirectX11();
		}
		return s_pInsatnce;
	}

	static void DeleteInstance()
	{
		if( s_pInsatnce )
		{
			SAFE_DELETE( s_pInsatnce );
		}
	}

	void ReadTexture( int texPage  );

	void SetCurrentOrientation(Windows::Graphics::Display::DisplayOrientations currentOrientation);
	void SetDpi(float dpi);
	void ValidateDevice();
	void HandleDeviceLost();
	void RegisterDeviceNotify(IDeviceNotify* deviceNotify);
	void Trim();

	void Present();
	void MakeSwapChane();

	void Init();
	void Update();
	void Render();

	// デバイス アクセサー。
	Windows::Foundation::Size GetOutputSize() const					{ return m_outputSize; }
	Windows::Foundation::Size GetLogicalSize() const				{ return m_logicalSize; }

	// D3D アクセサー。
	ID3D11Device2*			GetD3DDevice() const					{ return m_d3dDevice.Get(); }
	ID3D11DeviceContext2*	GetD3DDeviceContext() const				{ return m_d3dContext.Get(); }
	IDXGISwapChain3*		GetSwapChain() const					{ return m_swapChain.Get(); }
	D3D_FEATURE_LEVEL		GetDeviceFeatureLevel() const			{ return m_d3dFeatureLevel; }
	D3D11_VIEWPORT			GetScreenViewport() const				{ return m_screenViewport; }
	DirectX::XMFLOAT4X4		GetOrientationTransform3D() const		{ return m_orientationTransform3D; }

private:

	static CDirectX11 *s_pInsatnce;

	void init();
	void createDevice();
	void reset();

	void renderGameObject();
	void renderSystem();
	void makeConstantData();

	void configTextureSampling();
	void configBlendState();

	gxBool makeTexture( TextureData *pTexture , int w =2048, int h=2048 , int bitDepth=32 , Uint8 *pData=NULL , Uint32 uSize=0 );


	//テクスチャ追加
//	TextureData m_TexBasicBuff;
//	TextureData m_TexBackBuff;
//	TextureData m_TexGameScreen;
//	TextureData m_TexWallPaper;

	TextureData m_TextureData[ enTexturePageMax ];

	// Direct3D オブジェクト。
	Microsoft::WRL::ComPtr<ID3D11Device3>			m_d3dDevice;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext3>	m_d3dContext;
	Microsoft::WRL::ComPtr<IDXGISwapChain3>			m_swapChain;

	D3D11_VIEWPORT									m_screenViewport;

	// ウィンドウへのキャッシュされた参照 (省略可能)。
	//Platform::Agile<Windows::UI::Core::CoreWindow> m_window;

	// キャッシュされたデバイス プロパティ。
	D3D_FEATURE_LEVEL								m_d3dFeatureLevel;
	Windows::Foundation::Size						m_BackBufferSize;
	Windows::Foundation::Size						m_outputSize;
	Windows::Foundation::Size						m_logicalSize;
	Windows::Graphics::Display::DisplayOrientations	m_nativeOrientation;
	Windows::Graphics::Display::DisplayOrientations	m_currentOrientation;
	float											m_dpi;

	// 方向を表示するために使用される変換。
	DirectX::XMFLOAT4X4	m_orientationTransform3D;


private:

	void makeWallPaper();

	// デバイス リソースへのキャッシュされたポインター。
	//std::shared_ptr<CDirectX11> m_directX11;

	// キューブ ジオメトリの Direct3D リソース。
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	m_inputLayout;
	Microsoft::WRL::ComPtr<ID3D11Buffer>		m_vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>		m_indexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	m_vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	m_pixelShader;
	Microsoft::WRL::ComPtr<ID3D11Buffer>		m_constantBuffer;
	ConstantBufferForView3D m_ConstBuffer3dView;

	enum {
		enBlendTypeDefault,
		enBlendTypeAdd,
		enBlendTypeSub,
		enBlendTypeCross,
		enBlendTypeReverse,
		enBlendTypeXor,
		enBlendTypeScreen,
		enBlendMax,
	};
	enum {
		enSamplerMax = 2,
	};


	//各種ステート

	// テクスチャ用のサンプラー
	Microsoft::WRL::ComPtr<ID3D11SamplerState> m_SamplerState[enSamplerMax];

	Microsoft::WRL::ComPtr<ID3D11BlendState>	m_pBlendState[enBlendMax];

	Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_pRasterizerState;

	gxBool m_bUpConvert;
	gxBool m_bInitCompleted;
	gxBool m_b3DView;


};

// デバイスに依存しないピクセル単位 (DIP) の長さを物理的なピクセルの長さに変換します。
inline float ConvertDipsToPixels(float dips, float dpi)
{
	static const float dipsPerInch = 96.0f;
	return floorf(dips * dpi / dipsPerInch + 0.5f); // 最も近い整数値に丸めます。
}

#if defined(GX_DEBUG)
// SDK レイヤーのサポートを確認してください。
inline bool SdkLayersAvailable()
{
	HRESULT hr = D3D11CreateDevice(
		nullptr,
		D3D_DRIVER_TYPE_NULL,       // 実際のハードウェア デバイスを作成する必要はありません。
		0,
		D3D11_CREATE_DEVICE_DEBUG,  // SDK レイヤーを確認してください。
		nullptr,                    // どの機能レベルでも対応できます。
		0,
		D3D11_SDK_VERSION,          // Windows ストア アプリでは、これには常に D3D11_SDK_VERSION を設定します。
		nullptr,                    // D3D デバイスの参照を保持する必要はありません。
		nullptr,                    // 機能レベルを調べる必要はありません。
		nullptr                     // D3D デバイスのコンテキスト参照を保持する必要はありません。
		);

	return SUCCEEDED(hr);
}

#endif




#endif

