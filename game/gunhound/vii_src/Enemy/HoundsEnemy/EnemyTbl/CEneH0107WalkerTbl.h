enum {
 enTexPageBase = enTexCEneH0107Walker,
};
gxSprite SprCEneH0107Walker[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,0,0,72,16,24,16},//高射砲１
    {enTexPageBase+0,16,24,16,8,8,4},//高射砲２
    {enTexPageBase+0,72,0,64,40,32,8},//高射砲台
    {enTexPageBase+0,0,40,104,184,60,184},//操作室
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneH0107Walker[][16]={
    {160,200,0,0,0,0,100,100,100,0},//シーン
    {0,0,0,0,0,0,100,100,100,4},//操作室
    {-5,-193,-1,0,0,0,100,100,100,3},//高射砲台
    {-5,-5,0,0,0,0,100,100,100,2},//高射砲
    {0,0,0,0,0,0,100,100,100,1},//高射砲１
    {0,0,0,0,0,0,0,0,0,5},//0
};
enum {
    enPARENT,
    enSOUSASITU,
    enKOUSYAHOUDAI,
    enKOUSYAHOU1,
    enKOUSYAHOU2,
    enMax,
    
};
Sint32 m_sParentCEneH0107Walker[]={
    -1,
    enPARENT,
    enSOUSASITU,
    enKOUSYAHOUDAI,
    enKOUSYAHOU1,
    
};
