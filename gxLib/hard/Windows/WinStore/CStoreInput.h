﻿#ifndef _STORE_INPUT_H_
#define _STORE_INPUT_H_
//------------------------------------------------------
//
// コントローラー制御( XInput使用)
//
//------------------------------------------------------

class CStoreInput
{
public:
	enum {
		enControllerMax = 16,
	};

	CStoreInput();
	~CStoreInput();

	void Init();
	void Action();

	Uint32 GetDeviceNum()
	{
		return m_uGamingDeviceNum;
	}

	void CheckAccellarator( Sint32 id , gxBool bPush );

	typedef struct St360Pad {
		St360Pad()
		{
			Button = 0;
		}
		Sint32 Button;
	} St360Pad;

	St360Pad m_GamePad[32];

	SINGLETON_DECLARE( CStoreInput );

private:

	void UpdateGamingDevices(int n);			//ハードウェア情報を更新

	void rumble();

	//ハードウェアパッド情報
	CGameingDevice 	*m_pGamingDevice[enControllerMax];

	//検出されたパッドの数
	Uint32 m_uGamingDeviceNum;

	gxBool m_bUseController;
	gxBool m_bEnable[enControllerMax][GDBTN_MAX];

	enum {
		enID_NoneAccel ,			//
		enID_ChangeFullScreenMode ,//= 11001,	//フルスクリーン切り替え
		enID_AppExit              ,//= 11002,	//アプリ終了
		enID_GamePause			  ,//= 11003,	//ゲームのポーズ
		enID_GameStep			  ,//= 11004,	//ゲームのステップ
		enID_PadEnableSwitch	  ,//= 11005,	//コントローラー設定
		enID_DebugMode			  ,//= 11006,	//デバッグモードのON/OFF
		enID_Reset				  ,//= 11007,	//リセット
		enID_ScreenShot           ,//= 11008,	//スクリーンショット
		enID_FullSpeed			  ,//= 11009,	//フルスピード
		enID_SoundSwitch		  ,//= 11010,	//サウンドOn /Off
		enID_SamplingFilter		  ,//= 11011,	//サンプリングフィルター

		enID_MasterVolumeAdd	  ,//= 11012,	//Volume+
		enID_MasterVolumeSub	  ,//= 11013,	//Volume+
		enID_SwitchVirtualPad	  ,//= 11014,	//VirtualPad On/ Off
		enID_Switch3DView		  ,//= 11015,	//3DView
		enAccelMax			  	  ,//= 11,
	};

	Sint32 m_Accellarator[enAccelMax];

};


#endif
