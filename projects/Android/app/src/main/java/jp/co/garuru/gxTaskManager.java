//-------------------------------------------------------------------------------
//Timer関連　あまり速度にこだわらあないAndroidの希望を使うときに。。
//-------------------------------------------------------------------------------
package jp.co.garuru;
import java.util.TimerTask;
import android.app.Activity;
import android.os.Handler;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import static android.app.Activity.RESULT_OK;

public class gxTaskManager extends TimerTask {
	private Handler handler;
	private Context context;
	private Toast m_ToastTask;

	public gxTaskManager(Context context) {
		handler = new Handler();
		this.context = context;
		m_ToastTask = null;
	}

	@Override
	public void run() {
		handler.post(new Runnable() {
			@Override
			public void run()
			{
				//((GameGirlActivity)context).InvalidateScreen();
				if( GameGirlActivity.GetInstance().m_ToastString != "" )
				{
					if( m_ToastTask != null ) m_ToastTask.cancel();

					String text = GameGirlActivity.GetInstance().m_ToastString;
					m_ToastTask = Toast.makeText( ((GameGirlActivity)context), text, Toast.LENGTH_LONG );
					m_ToastTask.show();
					GameGirlActivity.GetInstance().m_ToastString = "";
				}

				byte[] gameGirlStates;
				gameGirlStates = GameGirlJNI.GetCppState();

				//-------------------------------------------------------------
				//BlueToothOn;
				//-------------------------------------------------------------
				if( (gameGirlStates[4]&(0x01<<0)) != 0 )
				{
					if( GameGirlActivity.GGA_ENABLE_BLUETOOTH == false )
					{
						GameGirlActivity.GGA_ENABLE_BLUETOOTH = true;
						GameGirlActivity.GetInstance().BlueToothInit();
					}
				}

				//-------------------------------------------------------------
				//Voice Input
				//-------------------------------------------------------------
				int n= 1;
				if( n == 0 )
				{
					//GameGirlActivity.GetInstance().InputVoice();
				}

				//-------------------------------------------------------------
				//WebView
				//-------------------------------------------------------------
				GameGirlActivity.GetInstance().WebViewProc();
			}
		});
	}

};
