enum {
 enTexPageBase = enCEneH0501PlasmaMachine,
};
gxSprite SprCEneH0501Battlecannon[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,0,0,96,32,32,16},//砲台
    {enTexPageBase+0,104,0,64,16,8,8},//砲塔
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneH0501Battlecannon[][16]={
    {160,200,0,0,0,0,100,100,100,0},//シーン
    {-15,-17,0,0,0,0,100,100,100,1},//砲台
    {70,-6,-1,0,0,0,100,100,100,2},//砲塔１
    {0,0,0,0,0,0,0,0,0,3},//0
};
enum {
    enPARENT,
    enHOUDAI,
    enHOUTOU,
    enMax,
    
};
Sint32 m_sParentCEneH0501Battlecannon[]={
    -1,
    enPARENT,
    enHOUDAI,
    
};
