﻿#ifndef _CXAUDIO2_H_
#define _CXAUDIO2_H_

#ifdef _USE_OPENAL

#else

static const int STREAMING_BUFFER_SIZE = 65536;
static const int MAX_BUFFER_COUNT = 3;

#define XAUDIO2_HELPER_FUNCTIONS 1
#include <xaudio2.h>
#include <xaudio2fx.h>

#include <mmreg.h>
#include <mfidl.h>
#include <mfapi.h>
#include <mfreadwrite.h>
#include <mfmediaengine.h>
#include "../../util/CFileWave.h"

class CXAudio;

class CAudioEngineCallbacks: public IXAudio2EngineCallback
{
private:
	CXAudio* m_pXAudio2;

public :
	CAudioEngineCallbacks(){};
	void Initialize(CXAudio* audio);

	// Called by XAudio2 just before an audio processing pass begins.
	void _stdcall OnProcessingPassStart(){};

	// Called just after an audio processing pass ends.
	void  _stdcall OnProcessingPassEnd(){};

	// Called when a critical system error causes XAudio2
	// to be closed and restarted. The error code is given in Error.
	void  _stdcall OnCriticalError(HRESULT Error);
};

typedef struct StSoundEffectData
{
	int						m_soundEventType;
	IXAudio2SourceVoice*	m_SourceVoice;
	XAUDIO2_BUFFER			m_AudioBuf;
	Uint32					m_uSampleRate;
	gxBool					m_bPlay;
	//CFileWave				*m_pFileWave;
	//Float32				m_fVolume;
} StSoundEffectData;


struct StStreamingVoiceContext : public IXAudio2VoiceCallback
{
	STDMETHOD_(void, OnVoiceProcessingPassStart)(UINT32){}

	STDMETHOD_(void, OnVoiceProcessingPassEnd)(){}

	STDMETHOD_(void, OnStreamEnd)(){}

	STDMETHOD_(void, OnBufferStart)(void*)
	{
		ResetEvent(hBufferEndEvent);
	}

	STDMETHOD_(void, OnBufferEnd)(void* pContext)
	{
		// Trigger the event for the music stream.
		if ( pContext == 0 )
		{
			SetEvent(hBufferEndEvent);
		}
	}
	STDMETHOD_(void, OnLoopEnd)(void*){}
	STDMETHOD_(void, OnVoiceError)(void*, HRESULT){}

	HANDLE hBufferEndEvent;

	StStreamingVoiceContext() : hBufferEndEvent(CreateEventEx(NULL, gxFalse, gxFalse, NULL))
	{

	}

	virtual ~StStreamingVoiceContext()
	{
		CloseHandle(hBufferEndEvent);
	}
};



class CXAudio
{
public:
	enum{
		enSoundMax = MAX_SOUND_NUM,
	};

	CXAudio()
	{
		m_pXAudioEngine = NULL;
		m_bEngineCriticalError = gxFalse;

		for (int i = 0; i < enSoundMax; i++)
		{
			//m_Sound[i].m_pSoundData 	= NULL;
			m_Sound[i].m_SourceVoice	= NULL;
			m_Sound[i].m_bPlay			= gxFalse;
			//m_Sound[i].m_pFileWave		= NULL;
			ZeroMemory( &m_Sound[i].m_AudioBuf, sizeof(m_Sound[i].m_AudioBuf ) );
		}

		m_fMasterVolume = 1.0f;
	}

	~CXAudio()
	{
		release();
		clear();
	}

	gxBool Init();

	void Action();

	void SetEngineExperiencedCriticalError()
	{
		m_bEngineCriticalError = true;
	}

	SINGLETON_DECLARE( CXAudio );

private:

	void playSoundEffect(int sound);
	void stopSoundEffect( int sound);
	void setSoundFrequency( int sound , Float32 ratio );
	void setPan( int sound , Float32 pan );
	void setReverb( int sound , gxBool bEnable );

	gxBool isPlay( int sound );
	void setVolume( Sint32 no , Float32 fVolume );
	void setMasterVolume( Float32 fMasterVolume );

	gxBool loadWavFile( Sint32 n , char* pFileName );
	gxBool readPCMFile( Sint32 n , Uint8 *pData , Uint32 uSize , StWAVEFORMATEX* pFormat );

	gxBool clear();
	void   release();

	IXAudio2MasteringVoice*	 m_XA2MasterVoice;
	IXAudio2*				 m_pXAudioEngine;
	StSoundEffectData		 m_Sound[ enSoundMax ];

	Float32 m_fMasterVolume;

	gxBool m_bEngineCriticalError;


	//--------------------

	CAudioEngineCallbacks	m_pXAudioEngineCallback;
	StStreamingVoiceContext	m_voiceContext;

};


#endif

#endif
