﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxDebug.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxNetworkManager.h>
#include <gxLib/gxFileManager.h>
#include "COpenGLES2.h"
//#include "CAudio.h"
#include "CGamePad.h"
#include "CMemory.h"

#include <time.h>


//save/load
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>


SINGLETON_DECLARE_INSTANCE( CDeviceManager );
SINGLETON_DECLARE_INSTANCE( CAndroid )

//#define CAudio COpenSLES
#define CAudio COpenAL

#define  LOG_TAG    "gxLib_jni"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

//#include "COpenSLES.h"
#include "COpenAL.h"

/*
Float32 getFrameTime()
{
	static Uint64 oldCount = 0;
	static Uint64 freq = 0;

	static Sint64 current = 3;

	timespec ts;
	clock_gettime( CLOCK_MONOTONIC, &ts );

    if( current > 0 )
    {
        current  --;
        return 0.0f;
    }

	if (oldCount == 0 )
	{
		oldCount = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	}

	Uint64 now = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	Uint64 sa = ( now - oldCount );

	Float32 fElapsedTime = (Float32)(sa/1000.0f);

	return fElapsedTime;
}
*/


AAssetManager* GetAssetManager()
{
	JNIEnv* env;

	//if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
	//{
	//	return NULL;
	//}
    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

	//クラス取得
	jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

	if (jc != 0)
	{
		jmethodID id = env->GetStaticMethodID( jc, "GetAssetManager", "()Ljava/lang/Object;");
		if( id != NULL )
		{
			jobject jobj = env->CallStaticObjectMethod(jc, id);

			AAssetManager* pAM;
			pAM = AAssetManager_fromJava( env, jobj );

			env->DeleteLocalRef(jobj);

			return pAM;
		}
	}

	return NULL;
}


CDeviceManager::CDeviceManager()
{

}


CDeviceManager::~CDeviceManager()
{
	
}

void CDeviceManager::AppInit()
{
	static int nnn = 0;
	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
		CAudio::GetInstance()->Init();
        COpenGLES2::GetInstance()->Init();
		MemoryInit();
	} else{
        CGameGirl::GetInstance()->SetResume();
    }


	nnn ++;
}

void   CDeviceManager::GameInit()
{
    //バックで受け取った入力機器の情報をgxLib側へ更新する
	CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
	static gxBool m_bGameInit = gxFalse;
	gxBool bExist = gxTrue;

	if( !m_bGameInit )
	{
		bExist = ::GameInit();
		m_bGameInit = gxTrue;
	}

	if( bExist )
	{
		//ゲームメインへ
		bExist = ::GameMain();
	}

	if( !bExist ) return gxFalse;

	return gxTrue;
}

void   CDeviceManager::Render()
{
	if( CGameGirl::GetInstance()->IsResume() ) return;

    COpenGLES2::GetInstance()->Update();
    COpenGLES2::GetInstance()->Render();
}


/*
void   CDeviceManager::vSync()
{
	static Float32 s_fOld = getFrameTime();
	Float32 fps = FRAME_PER_SECOND;

	while ( gxTrue )
	{
		Float32 fNow = getFrameTime();;

		if( fNow >= s_fOld + ( 1.0f / fps  ) )
		{
			s_fOld = fNow;
			break;
		}
	}
}
*/

void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = gxLib::GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = gxLib::GetTime();
	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


void   CDeviceManager::Flip()
{
	COpenGLES2::GetInstance()->Present();
}


void   CDeviceManager::Resume()
{
    COpenGLES2::DeleteInstance();
    COpenGLES2::GetInstance()->Init();
    gxTexManager::GetInstance()->UploadTexture(gxTrue);
}


void   CDeviceManager::Movie()
{
	
}


void   CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	static gxNetworkManager::StHTTP *http = NULL;

	if( http == NULL )
	{
		http = gxNetworkManager::GetInstance()->GetNextReq();
	}
	else
	{
		if( http->GetSeq() == 999 )
		{
			http = NULL;
		}
		return gxFalse;
	}

	if( http )
	{
		if( !http->bRequest )  return gxFalse;

		//------------
        http->bRequest = gxFalse;
        http->SetSeq ( 100 );

		CAndroid::GetInstance()->HttpRequest( http->index , http->m_URL );

//	    JNIEnv* env;
//
//	    if( CAndroid::GetInstance()->m_pJavaVM == NULL )
//	    {
//	       return gxFalse;
//	    }
//
//		CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
//
//	    //javaからパッケージ名を引っ張ってくる
//
//	    jclass jc = env->FindClass( JAVA_PROGRAM_PATH );
//
//	    if (jc != 0)
//	    {
//            jmethodID id = env->GetStaticMethodID( jc, "HTTPAccess", "(ILjava/lang/String;)I" );
//
//	        if( id != NULL )
//	        {
//
//                jstring str = env->NewStringUTF( http->m_URL );//"http://www.garuru.co.jp" );
//				int ret = (int)(env->CallStaticIntMethod( jc, id , http->index , str ) );
//	        }
//	    }
	}

	return gxTrue;

}

void   CDeviceManager::UploadTexture(Sint32 sBank)
{
    COpenGLES2::GetInstance()->ReadTexture( sBank );
}

void   CDeviceManager::LogDisp(char* pString)
{
	//__android_log_print(ANDROID_LOG_WARN , APPLICATION_NAME , "%s" , pString );
    __android_log_print(ANDROID_LOG_ERROR , APPLICATION_NAME , "%s" , pString );

}

void CDeviceManager::Clock( gxLib::Clock *pClock )
{
	//現在の時刻をミリ秒で取得する
	std::chrono::system_clock::time_point now;
	now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(now);
	tm local_tm = *localtime(&tt);

	std::chrono::system_clock::duration tp = now.time_since_epoch();
	std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tp);

	pClock->Year  = local_tm.tm_year + 1900;	// years since 1900
	pClock->Month = local_tm.tm_mon + 1;		// months since January - [0, 11]
	pClock->Day   = local_tm.tm_mday;   		// day of the month - [1, 31]
	pClock->DOW   = local_tm.tm_wday; 			// days since Sunday - [0, 6]
	pClock->Hour  = local_tm.tm_hour; 			// hours since midnight - [0, 23]
	pClock->Min   = local_tm.tm_min; 			// minutes after the hour - [0, 59]
	pClock->Sec   = local_tm.tm_sec; 			// seconds after the minute - [0, 60] including leap second
	pClock->MSec = (us.count()/1000) % (1000);
	pClock->USec = us.count() % (1000);

}

/*
Float32 CDeviceManager::Clock()
{
	timespec ts;
	tm st;
	// 時刻の取得
	clock_gettime(CLOCK_REALTIME, &ts);  //時刻の取得
	localtime_r( &ts.tv_sec, &st);       //取得時刻をローカル時間に変換
	
	
	Sint32 Year  = st.tm_year+1900;
	Sint32 Month = st.tm_mon+1;
	Sint32 Day   = st.tm_mday;
	//Sint32 time_st->tm_wday;

	Sint32 Hour = st.tm_hour;
	Sint32 Min  = st.tm_min;
	Sint32 Sec  = st.tm_sec;
	Sint32 MSec = ts.tv_nsec;
	Sint32 USec = ts.tv_nsec;

	//CGameGirl::GetInstance()->SetTime( Year , Month , Day , Hour , Min , Sec , MSec , USec );

	return getFrameTime();
}
*/


Uint8* loadFile( const gxChar* pFileName , Uint32* pLength , Uint32 location )
{
	//assetからデータを読み出す(read only)

	Uint32 len = strlen(pFileName);

	gxChar *fileName0 = new gxChar[ 32+len ];

	len = sprintf( fileName0 , "%s" , pFileName );

	gxBool bDirect = gxFalse;

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '\\' )
		{
			fileName0[ii] = '/';
		}

		if( fileName0[ii] == ':' )
		{
			bDirect = gxTrue;
		}
	}

	gxChar fileName[1024];

	if ( bDirect )
	{
		sprintf( fileName, "%s", fileName0 );
	}
	else
	{
		switch( location ){
            case STORAGE_LOCATION_CARD:
            {
                // data/data/jp.co.garuru/fileName
                sprintf( fileName, "/data/data/" );
                strcat ( fileName, CAndroid::GetInstance()->GetPackageName() );
                strcat ( fileName, "/" );
                strcat ( fileName, pFileName );
            }
            break;

		case STORAGE_LOCATION_ROM:
			{
				sprintf( fileName, "%s", pFileName );

			    AAssetManager *am = GetAssetManager();
			    CAndroid::GetInstance()->SetAssetManager( am );
			    AAsset *_asset = AAssetManager_open( am , fileName, AASSET_MODE_BUFFER );

			    if (_asset == NULL)
			    {
					LOGE("[Error:Asset Not Found :%s\n" , pFileName );
			    }
			    else
			    {
			        size_t _size = AAsset_getLength(_asset);     		  	// ファイルサイズ
			        static Uint8* _buf;
			        _buf = new unsigned char[_size];						// データバッファ
			        AAsset_read(_asset, (void*)_buf, _size);               	// データ読み込み
			        AAsset_close(_asset);
			        *pLength = _size;
					SAFE_DELETES( fileName0 );
			        return _buf;
			    }

				//sprintf( fileName, "Storage\\assets\\%s", fileName0);
			}
			break;

		case STORAGE_LOCATION_DISC:
			{
				// obb:fileName
				while( !CAndroid::GetInstance()->IsCheckOBB() )
				{
					gxLib::Sleep(100);
				}

				gxChar *pOBBPath = NULL;

				pOBBPath = CAndroid::GetInstance()->GetOBB_Path( 0 );

				if( pOBBPath == NULL )
				{
					//obbがマウントされていなかった
					LOGE("[Error:OBB Not Mounted :%s\n" , pFileName );
					return NULL;
				}
				sprintf( fileName , "%s/%s" , pOBBPath , fileName0 );
			}
			break;


		case STORAGE_LOCATION_DIRECT:		//				//読み書き、取り出しOK
			{
				// external SD / internal SD
				char filepath[512];
			    sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
				strcat( filepath, pFileName );
				sprintf( fileName, "%s"  , fileName0);
                //sprintf( fileName, "/data/data/" );
                //strcat ( fileName, CAndroid::GetInstance()->GetPackageName() );
                //strcat ( fileName, "/" );
                //strcat ( fileName, pFileName );
			}
			break;

		default:
			break;
		}
	}

	Sint32 fh = open((char*)fileName,O_RDONLY);

	if( fh < 0 )
	{
		//ファイルが開けなかった
		//gxLib::DebugLog("[Error:File Not Found:%s\n" , pFileName );	//別の場所で再挑戦して見つかる場合もあるのでエラーログをはかないようにしておく
		return NULL ;
	}
	else
	{
		struct stat filestat;

		fstat(fh,&filestat);
		size_t sz = filestat.st_size;

		Uint8 *pBuffer = new Uint8[ sz ];

		read(fh,pBuffer,sz );

		close( fh );

        *pLength = sz;
        return pBuffer;
	}
}


#include <gxLib/util/CFileZip.h>

Uint8    *g_pZipBuffer = NULL;
CFileZip *g_pAssetZip  = NULL;
gxChar m_ZipPath[FILENAMEBUF_LENGTH]={0};	//TODO::あとで開放すること

Uint8* getZipFileData( gxChar* pZipName , gxChar* pFileName , Uint32 *pLength , Uint32 location )
{
	Uint32 len = strlen( pFileName );

	gxChar *fileName0 = new gxChar[32 + len];

	len = sprintf(fileName0, "%s", pFileName);

	//zip時は / でパスを区切る

	for (int ii = 0; ii<len; ii++)
	{
		if (fileName0[ii] == '\\')
		{
			fileName0[ii] = '/';
		}
	}

	if( pLength )
	{
		*pLength = 0;
	}

	//------------------------------------
	//int fh;

	//Uint8* pBuffer = NULL;
	//struct stat filestat;
	//long sz, readsz;
	//unsigned long pos = 0;
	//int ret = 1;

	//-----------------------------------------

	Uint8 *pReturnData = NULL;
	gxBool bMakeNewZip = gxFalse;

	if( g_pAssetZip )
	{
		if( strcmp( m_ZipPath , pZipName ) != 0 )
		{
			bMakeNewZip = gxTrue;
			SAFE_DELETE( g_pAssetZip );
			SAFE_DELETES( g_pZipBuffer );

		}
	}
	else
	{
		bMakeNewZip = gxTrue;
	}

	if( bMakeNewZip )
	{
		Uint32 uSize = 0;
		Uint8 *pBuffer = loadFile( pZipName , &uSize , location );

		g_pZipBuffer = pBuffer;

		g_pAssetZip = new CFileZip();

		if ( !g_pAssetZip->Read( pBuffer , uSize ) )
		{
			//そもそもzipがなかった
			SAFE_DELETES( pBuffer );
			SAFE_DELETES( fileName0 );
			return NULL;
		}

		//最後に読んだZIP名を覚えておく
		sprintf( m_ZipPath ,"%s" , pZipName );
	}

	//Zipは読めている

	pReturnData = g_pAssetZip->Decode( fileName0, pLength );

	//SAFE_DELETE( pZip );		//使い回しがあるのでここでは消さない
	//SAFE_DELETES( pBuffer );	//使い回しがあるのでここでは消さない

	SAFE_DELETES( fileName0 );


	return pReturnData;
}


Uint8* CDeviceManager::LoadFile( const gxChar* pFileName , Uint32* pLength , Uint32 location )
{

	Uint32 len = strlen(pFileName);

	gxChar fileName0[ 1024 ];

	len = sprintf( fileName0 , "%s" , pFileName );

	gxBool bDirect = gxFalse;

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '\\' )
		{
			fileName0[ii] = '/';
		}

		if( fileName0[ii] == ':' )
		{
			bDirect = gxTrue;
		}
	}

#ifdef FILE_FROM_ZIP
	if( !bDirect )
	{
		Uint8 *pRetData = NULL;

		gxChar zipName[ 1024 ];

		switch( location ){
		case STORAGE_LOCATION_ROM:
			sprintf( zipName, "assets.zip");
			pRetData = getZipFileData( zipName , fileName0 , pLength , STORAGE_LOCATION_ROM );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case STORAGE_LOCATION_DISC:
			sprintf( zipName, "disc.zip");
			pRetData = getZipFileData( zipName , fileName0 , pLength , STORAGE_LOCATION_DISC );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case STORAGE_LOCATION_CARD:
		case STORAGE_LOCATION_DIRECT:
			break;
	
		default:
			break;
		}
	}

#endif
    return loadFile( pFileName , pLength , location );

//	else
//	{
////		sprintf(fileName, "Storage/disc/%s", fileName0);
//		sprintf(fileName, "%s", fileName0);
//	}
//
//    //-----------------------------------------------------
//    //まずはAssetを捜索
//    //-----------------------------------------------------
//
//    AAssetManager *am = GetAssetManager();
//    CAndroid::GetInstance()->asset_manager = am;
//    AAsset *_asset = AAssetManager_open( am , fileName, AASSET_MODE_BUFFER );
//
//    if (_asset == NULL)
//    {
//		LOGE("[Error:Asset Not Found :%s\n" , pFileName );
//    }
//    else
//    {
//        size_t _size = AAsset_getLength(_asset);       // ファイルサイズ
//        static Uint8* _buf;
//        _buf = new unsigned char[_size];//)malloc(_size);                   // データバッファ
//        AAsset_read(_asset, (void*)_buf, _size);               // データ読み込み
//        AAsset_close(_asset);
//        *pLength = _size;
//
//		SAFE_DELETES( fileName0 );
//        return _buf;
//    }
//
//    //-----------------------------------------------------
//	//assetになかったのでOBBを確認しに行く
//    //-----------------------------------------------------
//
//	while( !CAndroid::GetInstance()->IsCheckOBB() )
//	{
//		gxLib::Sleep(100);
//	}
//
//	gxChar *pOBBPath = NULL;
//
//	pOBBPath = CAndroid::GetInstance()->GetOBB_Path( 0 );
//
//	if( pOBBPath == NULL )
//	{
//		//obbがマウントされていなかった
//		LOGE("[Error:OBB Not Mounted :%s\n" , pFileName );
//		return NULL;
//	}
//
//	sprintf( fileName , "%s/%s" , pOBBPath , fileName0 );
//
//	SAFE_DELETES( fileName0 );
//
//	Sint32 fh = open((char*)fileName,O_RDONLY);
//
//
//	if(fh<0)
//	{
//		//OBBでもなかった
//		//close(fh);
//		LOGE("[Error:File Not Found int OBB:%s\n" , pFileName );
//		return NULL ;
//	}
//	else
//	{
//		struct stat filestat;
//		fstat(fh,&filestat);
//		size_t sz = filestat.st_size;
//
//		Uint8 *pBuffer = new Uint8[ sz ];
//
//		read(fh,pBuffer,sz );
//
//		close( fh );
//
//        *pLength = sz;
//        return pBuffer;
//	}

	return NULL;
}

bool CreateDirectories(char* pURL )
{
	//フォルダを作成する
	//とりあえずわたってきた名前をそのまま使う
	//Write時には２回（sjis,u8の２回処理が来ることに留意）

	gxBool bSuccess = gxTrue;

	std::vector<char*> separates;
	std::string url = pURL;;
	std::replace(url.begin(), url.end(), '\\', '/' );

	char *pBuf = new char[ url.size()+1];
	sprintf( pBuf, "%s", url.c_str());

	Sint32 cnt = 0;

	for ( Sint32 ii= url.size(); ii>=0; ii-- )
	{
		if ( pBuf[ii] == ':')
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( ii == 0 )
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( pBuf[ii] == '/')
		{
			pBuf[ii] = 0x00;
			if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
			cnt++;
		}
	}

	size_t max = separates.size();
	std::string path = "";

	for (Sint32 ii = 0; ii < max; ii++)
	{
		path += separates[max - 1 - ii];

		if (!mkdir( path.c_str(), S_IRWXU ))
		{
			bSuccess = gxFalse;
		}

		path += "/";
	}

	delete[] pBuf;

	return bSuccess;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize , Uint32 location )
{
    char filepath[512];



    // mnt/sd/external/fileName
    //char filepath[512];
    //strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
    //strcat( filepath, "/files/" );
    //strcat( filepath, "files/" );

    switch( location ){
        case STORAGE_LOCATION_ROM:
            return gxFalse;

        case STORAGE_LOCATION_DISC:
            return gxFalse;

        case STORAGE_LOCATION_CARD:
            //SaveStorageFile
            sprintf( filepath, "/data/data/" );
            strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
            strcat( filepath, "/" );
            strcat( filepath, pFileName );
            break;


        case STORAGE_LOCATION_DIRECT:
            //SaveFile://SD
            sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
            strcat( filepath, pFileName );
            break;

        default:
            break;
    }

    int fh;

    fh = open( (char*)filepath , O_WRONLY|O_CREAT , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

    if( fh < 0 )
    {
        //書き込みミス

		//ディレクトリを作って再度試す

		CreateDirectories( (char*)filepath );

		{
		    fh = open( (char*)filepath , O_WRONLY|O_CREAT , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

		    if( fh < 0 )
		    {
				return gxFalse;
			}
		}
    }

	write(fh,pWritedBuf,uSize);

    close( fh );

    return gxTrue;
}

//ストレージへのファイルアクセス
/*
gxBool CDeviceManager::SaveStorageFile( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize )
{
	//SDカードにデータを保存する

	char filepath[512];
    sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
	//strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
	//strcat( filepath, "/" );
	strcat( filepath, pFileName );

	int fh;

	fh = open((char*)filepath,
		O_WRONLY|O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);

	if(fh<0)
	{
		//書き込みミス
		return gxFalse;
	}
	else
	{
		write(fh,pWritedBuf,uSize);
	}

	close( fh );

	return gxTrue;
}


Uint8* CDeviceManager::LoadStorageFile( const gxChar* pFileName , Uint32* pLength )
{
	int fh;
	long sz_zero;
	long sz,readsz;
	unsigned long pos=0;
	int ret=1;
	struct stat filestat;
	Uint8* pBuffer=NULL;

	char filepath[512];

	sprintf( filepath, "/data/data/" );
	strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
//	strcat( filepath, "/files/" );
	strcat( filepath, "/" );
	strcat( filepath, pFileName );

	fh = open((char*)filepath,O_RDONLY);

	if(fh<0)
	{
		//読み込みミス
		//close(fh);
		return NULL;
	}
	else
	{
		fstat(fh,&filestat);
		readsz = sz = sz_zero = filestat.st_size;

		*pLength = filestat.st_size;
		pBuffer = new Uint8[ readsz ];

		if( pBuffer == NULL ) return NULL;

		while(ret > 0)
		{
			if( readsz > 1024 ) readsz = 1024; 

			ret = read(fh,&pBuffer[pos],readsz);
			pos += ret;
			sz -= ret;
			readsz = sz;
		}

		return pBuffer;
	}

	close( fh );

	return NULL;
}
*/

gxBool CDeviceManager::LoadConfig()
{
	return gxFalse;
}

gxBool CDeviceManager::SaveConfig()
{
	return gxFalse;
}

//特殊

#include <iostream>
#include<fstream>
//#include <fcntl.h>
//#include<sys/stat.h>
//#include<sys/types.h>
//#include <unistd.h>

#if 0
static void (*s_pFunc)(void*);
static void *thread_func(void *arg)
{
	if (CAndroid::GetInstance()->m_pJavaVM )
	{
		JNIEnv *env;
		// attach current thread to JavaVM and get pointer to JNIEnv
		CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
		//----------------------------------------------------------------------
		
		
		// do something here ここで処理を行う

		s_pFunc(arg);
		
		//----------------------------------------------------------------------
		// detach current thread from JavaVM
		CAndroid::GetInstance()->m_pJavaVM->DetachCurrentThread();
	}

	pthread_exit( NULL );
}
#endif

static pthread_t test_thread;

void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
//	s_pFunc = pFunc;
//	pthread_create( &test_thread, NULL, thread_func, (void *)pArg);

	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);

}

void CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}

gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxFalse;
}


void CDeviceManager::UpdateMemoryStatus(Uint32* uNow, Uint32* uTotal, Uint32* uMax)
{
	Uint32 uNowByte;
	Uint32 uTotalByte;
	Uint32 uMaxByte;

	::UpdateMemoryStatus(&uNowByte, &uTotalByte, &uMaxByte);

	*uNow = (uNowByte >> 10) >> 10;
	*uTotal = (uTotalByte >> 10) >> 10;
	*uMax = (uMaxByte >> 10) >> 10;

}


void CDeviceManager::ToastDisp( gxChar* pString )
{
	//Toastテキストを表示する

    JNIEnv* env;

    if( CAndroid::GetInstance()->m_pJavaVM == NULL )
    {
       return;
    }

    //if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
	//if( CAndroid::GetInstance()->GetJavaEnv() == 0 )
    //{
    //    return;
    //}

    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

    if (jc != 0)
    {
        jmethodID id = env->GetStaticMethodID(jc, "SetToastText", "(Ljava/lang/String;)V");
        //if( id != 0 )
        {
            //static jchar buf[FILENAMEBUF_LENGTH];
            //int len = 0;
            //len = sprintf( (char*)buf , "%s",pString);
            const char* pStr = pString;

            jstring str = env->NewStringUTF( pStr );
            env->CallStaticVoidMethod(jc, id ,str );
        }
    }
}

void CDeviceManager::OpenWebClient( gxChar* pString )
{
    //WebViewを表示するリクエストを発行する

    sprintf( CAndroid::GetInstance()->m_WebViewURLString,"%s" , pString );

#if 0
    if( m_WebViewURLString[0] )
    {
        JNIEnv* env;

        if( CAndroid::GetInstance()->m_pJavaVM == NULL )
        {
            return;
        }

        //if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
        //if( CAndroid::GetInstance()->GetJavaEnv() == 0 )
        //{
        //    return;
        //}

        CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

        jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

        if (jc != 0)
        {
            jmethodID id = env->GetStaticMethodID(jc, "SetWebViewURL", "(Ljava/lang/String;)V");
            //if( id != 0 )
            {
                //static jchar buf[FILENAMEBUF_LENGTH];
                //int len = 0;
                //len = sprintf( (char*)buf , "%s",pString);
                const char* pStr = pString;

                jstring str = env->NewStringUTF( pStr );
                env->CallStaticVoidMethod(jc, id ,str );
            }
        }
    }
#endif
}


wchar_t* CDeviceManager::UTF8toUTF16( gxChar*  pString  , size_t* pSize )
{
	//Androidではこれで問題なし

	setlocale(LC_ALL, "JPN");
	static wchar_t destBuf[FILENAMEBUF_LENGTH];

	mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );

	return destBuf;
};


wchar_t* CDeviceManager::SJIStoUTF16( gxChar*  pString  , size_t* pSize )
{
//    setlocale(LC_ALL, "JPN");
//    static wchar_t destBuf[FILENAMEBUF_LENGTH];
//
//    mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );
//
//    return destBuf;
	return NULL;
};


gxChar*  CDeviceManager::UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize )
{
	setlocale(LC_ALL, "JPN");
	static char destBuf[FILENAMEBUF_LENGTH];

	wcstombs( destBuf, pUTF16buf , FILENAMEBUF_LENGTH );

	return destBuf;
};


gxChar*  CDeviceManager::UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize )
{
	return NULL;
};

gxChar*  CDeviceManager::UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize )
{
	return NULL;
};

gxChar*  CDeviceManager::SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize )
{
	return NULL;
};

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}



