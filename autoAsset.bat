rem Android --------------------------------------------------

rem ZIPアーカイブシステムを使うとき
rem xcopy /Y Storage\assets.zip projects\Android\app\src\main\assets\
rem xcopy /Y Storage\disc.zip   projects\Android\app\src\main\assets\

xcopy /E /Y Storage\assets\*.* projects\Android\app\src\main\assets\
rem xcopy /Y Storage\disc.zip   projects\Android\app\src\main\assets\


rem Windows10 uwp --------------------------------------------------
rem Icon類をAssetsフォルダにコピーする
xcopy /Y projects\CommonFiles\uwp\*.png projects\Windows10UWP\Assets\

rem asset リソースをZipアセットとしてコピーする
xcopy /Y Storage\assets.zip projects\Windows10UWP\Assets\
xcopy /Y Storage\disc.zip   projects\Windows10UWP\Assets\
