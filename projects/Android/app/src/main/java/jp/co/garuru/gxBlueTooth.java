/**
 * Created by ragi on 2017/03/20.
 */

package jp.co.garuru;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.os.Bundle;
import android.view.WindowManager;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import java.util.Set;
import android.content.IntentFilter;
import android.content.Context;

import java.io.IOException;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import java.io.IOException;
import android.content.Intent;
import java.util.UUID;

import java.io.*;//ByteArrayOutputStream;
import android.os.AsyncTask;

import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;

public class gxBlueTooth extends Activity  {

    private BluetoothAdapter mBluetoothAdapter = null;
    private int m_SelectedMachineID = -1;
    private boolean m_bEnableBlueTooth = false;
    private BluetoothDevice m_BTDevice;
    private byte[] m_ByteData = new byte[64];

    Activity m_Activity;

    gxBlueTooth()
    {
        m_bEnableBlueTooth = false;
    }

    public void SetHandle( Activity activity)
    {
        m_Activity = activity;
    }

    public int Init()
    {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null)
        {
			//BT機能そのものがない
			System.out.println("BlueTooth機能がない");
			return -1;
        }

        if (!mBluetoothAdapter.isEnabled())
        {
			// BT無効
			System.out.println("BlueToothが無効である");

            return 0;
        }

        return 1;
    }

    public void Start()
    {
        m_bEnableBlueTooth = true;

        //ブルートゥーススレッド開始

        Runnable sendTask = new BTMainThread(m_Activity);
        new Thread(sendTask).start();
    }

    private BluetoothSocket clientSocket;

    int mDeviceCnt = 0;
    private static String[]  m_BTDeviceNameArray;
    private BluetoothDevice[] m_BTDeviceArray;


    private static String[]  _BTDeviceNameArray = new String[128];
    private static BluetoothDevice[] _BTDeviceArray = new BluetoothDevice[128];
    int _DeviceCnt = 0;

    public void AddDeviceList( BluetoothDevice  foundDevice)
    {
        _BTDeviceNameArray[_DeviceCnt] = foundDevice.getName();
        _BTDeviceArray[_DeviceCnt] = foundDevice;
        _DeviceCnt++;

    }

    public void MakeDeviceList()
    {
        m_BTDeviceNameArray = new String[_DeviceCnt];
        m_BTDeviceArray = new BluetoothDevice[_DeviceCnt];
        mDeviceCnt = _DeviceCnt;

        for( int ii=0;ii<mDeviceCnt;ii++)
        {
            m_BTDeviceNameArray[ii] = _BTDeviceNameArray[ii];
            m_BTDeviceArray[ii] = _BTDeviceArray[ii];
        }
    }

    public void SetEnable()
    {
        m_bEnableBlueTooth = true;
    }

    public boolean IsExistPairedMachine()
    {
		//ペアリングされた機器を探す
		
        Set<BluetoothDevice> pairedDevice = mBluetoothAdapter.getBondedDevices();

        mDeviceCnt = 0;

        if( pairedDevice.size() > 0 )
        {
			//過去にペアリングされた機器があれば一覧を作る
			
			int sz = pairedDevice.size();

            m_BTDeviceNameArray = new String[ sz ];
            m_BTDeviceArray	    = new BluetoothDevice[ sz ];

            for (BluetoothDevice device : pairedDevice)
            {
                m_BTDeviceNameArray[mDeviceCnt] = device.getName();
                m_BTDeviceArray[mDeviceCnt]      = device;
                mDeviceCnt++;
            }

            DiscoveryCancel();

            SelectPairingMachine("機器選択");

            return true;
        }
        else
        {
            DiscoveryCancel();

            mBluetoothAdapter.startDiscovery();
            System.out.println("ペアリングできる機器を探しに行く");
        }

		return false;
    }

    public void DiscoveryCancel()
    {
        if ( mBluetoothAdapter.isDiscovering() )
        {
            mBluetoothAdapter.cancelDiscovery();
        }
    }


    public void SelectPairingMachine(String caption)
    {
		//ＢＴ機器を選択する

        new AlertDialog.Builder(m_Activity)
                .setTitle( caption)
                .setItems(m_BTDeviceNameArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // item_which pressed
                        m_SelectedMachineID = which;
                    }
                })
                .show();
    }

    int m_RetryConnect = 0;
    public void RetryConnect()
    {
        m_RetryConnect = 0;
        new AlertDialog.Builder(m_Activity)
                .setTitle("Connect")
                .setMessage("もう一度コネクトしますか？")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // OK button pressed
                        m_RetryConnect = 1;
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // OK button pressed
                        m_RetryConnect = -1;
                    }

                })
                .show();

    }

	public boolean IsDataExist()
    {
		//byte[] sendData = GameGirlActivity.GetInstance().BTGetSendData();
        byte[] sendData = BTGetSendData();

		if( sendData == null )
		{
			return false;
		}

		//ヘッダー、フッターを作る

		int length = sendData.length;
		byte[] data = new byte[length+2+4+2];

		data[0] = 'G';
		data[1] = 'X';
		data[2] = (byte)length;
		data[3] = 0;
		data[4+length+0] = 'B';
		data[4+length+1] = 'T';

		for( int ii=0; ii<length; ii++ )
		{
			data[4+ii] = sendData[ii];
		}

		m_ByteData = data;

		return true;
    }

	BroadcastReceiver mReceiver;
	public void SearchBluetoothDevice()
	{
		//検出されたデバイスからのブロードキャストを受ける

		mReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				String dName = null;
				BluetoothDevice foundDevice;
				if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
				{

				}

				if (BluetoothDevice.ACTION_FOUND.equals(action))
				{
					//デバイスが検出された
					foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

					if ((dName = foundDevice.getName()) != null)
					{
						if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED)
						{
							//接続したことのないデバイスのみアダプタに詰める
							//nonPairedDeviceAdapter.add(dName + "\n" + foundDevice.getAddress());
							//Log.d("ACTION_FOUND", dName);

							AddDeviceList( foundDevice );//, foundDevice.getName() );
							//m_BTDeviceNameArray[mDeviceCnt] = foundDevice.getName();
							//m_BTDeviceArray[mDeviceCnt] = foundDevice;
							//mDeviceCnt++;

						}
					}
					//nonpairedList.setAdapter(nonPairedDeviceAdapter);
				}
				if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
					//名前が検出された
					//Log.d("ACTION_NAME_CHANGED", dName);
					foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
						//接続したことのないデバイスのみアダプタに詰める
						//nonPairedDeviceAdapter.add(dName + "\n" + foundDevice.getAddress());
//					m_BTDeviceNameArray[mDeviceCnt] = foundDevice.getName();
//					m_BTDeviceArray[mDeviceCnt]	  = foundDevice;
						AddDeviceList( foundDevice );//, foundDevice.getName() );
					}
					//nonpairedList.setAdapter(nonPairedDeviceAdapter);
				}

				if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
				{
					//スキャン終了
					DiscoveryCancel();

					if( mReceiver != null )
					{
						unregisterReceiver(mReceiver);
					}

					MakeDeviceList();

					SelectPairingMachine("ペアリングする機器を選択してください");
				}
			}
		};

		// インテントフィルタの作成
		//IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		//			registerReceiver(searchBluetoothDevice, filter);
		// ブロードキャストレシーバの登録
		registerReceiver(mReceiver , filter);
	};

    public byte[] m_SendData = new byte[256];
    public byte[] m_RecvData = new byte[256];

    public void StoreReceivedData( byte[] recvData )
    {
        //recvしたデータをgxLibにセットする

        for( int ii=0; ii<recvData.length; ii++ )
        {
            m_RecvData[ii] = recvData[ii];
        }
        GameGirlJNI.SetBTData( m_RecvData , recvData.length );

    }

    public byte[] BTGetSendData()
    {
        //sendするデータをgxLibから取得する

        byte[] data;

        data = GameGirlJNI.GetBTData();

        if (data == null)
        {
            m_SendData = null;
            return null;
        }

        m_SendData = new byte[data.length];

        for( int ii=0; ii<data.length; ii++ )
        {
            m_SendData[ii] = data[ii];
        }

        return m_SendData;
    }


    //--------------------------------------------------------------
	//BlueToothスレッドｘ２
	//--------------------------------------------------------------

    public static final UUID BLUETOOTH_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket mmSocket;
    private BluetoothServerSocket mmServSocket;
    private  OutputStream mmOutStream;
    private  InputStream mmInStream;

    private int m_Seq = 0;

    class BTMainThread implements Runnable
	{
        Context m_Context;
		BTMainThread(Context context )
		{
			m_Context = context;
		}

        @Override
        public void run()
        {
            boolean bLoop = true;

            while ( bLoop )
            {
                if ( Connect() )
                {
                    bLoop = true;
                }

                try {
                    Thread.sleep( 1000 );
                } catch (InterruptedException e) {
                    //Log.e(TAG, "Exception during write", e);
                }
            }
        }
        private BluetoothGatt mBleGatt;
        private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                // 接続状況が変化したら実行.
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    // 接続に成功したらサービスを検索する.
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    // 接続が切れたらGATTを空にする.
                    if (mBleGatt != null) {
                        mBleGatt.close();
                        mBleGatt = null;
                    }
                    //mIsBluetoothEnable = false;
                }
            }
        };

        private boolean Connect()
        {
			// ペアリングされたマシンを選択されるまで待つ

			while (true)
			{
				if ( m_bEnableBlueTooth == true )
				{
					if ( m_SelectedMachineID != -1 )	break;
				}

				try
				{
					Thread.sleep(100);
				}
				catch (InterruptedException e)
				{
					//Log.e(TAG, "Exception during write", e);
				}
			}

			//デバイスと接続する

			m_BTDevice = m_BTDeviceArray[m_SelectedMachineID];

			boolean bLoop = true;

            try {
                mmSocket      = m_BTDevice.createRfcommSocketToServiceRecord( BLUETOOTH_UUID );
            } catch (IOException e) {
                //return false;
            }

            while( bLoop )
			{
				boolean bNoError = true;

	            try {
	                // This is a blocking call and will only return on a
	                // successful connection or an exception
                    mmSocket.connect();
	            } catch (IOException e) {
	                //connectionFailed();
	                // Close the socketzzz
					bNoError = false;
    			}

				if( bNoError == false )
				{
/*
//                    try {
//                        // This is a blocking call and will only return on a
//                        // successful connection or an exception
//                        //mmServSocket.accept();
//                    } catch (IOException e) {
//                        //connectionFailed();
//                        // Close the socketzzz
//                        bError = true;
//                        m_RetryConnect = -1;
//                        continue;
//                    }
*/
				}
                else
                {
                    //接続成功
                    break;
                }
			}

	        //接続完了時の処理
	        //ReadWriteModel rw = new ReadWriteModel(mContext, clientSocket, myNumber);
	        //rw.start();

            //データの受け渡し

            OutputStream tmpOut = null;
            InputStream tmpIn = null;

            try {
                tmpIn  = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                //Log.e(TAG, "temp sockets not created", e);
            }

            mmOutStream = tmpOut;
            mmInStream  = tmpIn;

            byte num = 0;

	        Runnable receiveTask = new ReceiveData();
	        new Thread( receiveTask ).start();

			m_Seq = 0;

			GameGirlActivity.GetInstance().SetToastText("BlueTooth Plugin.");

            while( m_Seq < 1000 )
            {
                num ++;

				while( true )
				{
					if( !IsDataExist() ) break;

	                byte[] buffer = new byte[m_ByteData.length];
					System.arraycopy(m_ByteData , 0 , buffer ,0 ,  m_ByteData.length );

	                try {
						//送信
	                    mmOutStream.write(buffer);

	                    // Share the sent message back to the UI Activity
	                    //mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
	                    m_Seq = 800;

	                } catch (IOException e) {
						//エラー
						m_Seq = 1000;
	                    //Log.e(TAG, "Exception during write", e);
	                }

/*
//                    buffer = new byte[256];
//					//ここでReceive待ちするとデータがくるまで次に行けない
//                    try {
//                        //受信
//                        mmInStream.read(buffer);
//                        System.arraycopy(buffer , 0 , m_ByteData ,0 ,  buffer.length );
//
//                        m_Seq = 800;
//
//                    } catch (IOException e) {
//                        //エラー
//                        m_Seq = 1000;
//                        //Log.e(TAG, "Exception during write", e);
//                    }
*/
				}

                try {
                    Thread.sleep(1000/120);
                } catch (InterruptedException e) {
                    //Log.e(TAG, "Exception during write", e);
                }
            }

            //接続終了

            try {
                mmSocket.close();
            } catch (IOException e2) {
                return true;//Log.e(TAG, "unable to close() socket during connection failure", e2);
            }
            GameGirlActivity.GetInstance().SetToastText("BlueTooth Plugout.");
            return true;
        }
    }


    class ReceiveData implements Runnable
	{
        //dataを受信した

        @Override
        public void run()
        {
            boolean bLoop = true;

            while( bLoop )
            {
                //if( !IsDataExist() ) break;

                byte[] buffer = new byte[ 256 ];

                try {
                    //受信
                    mmInStream.read( buffer );

                    int sz;

                    if( buffer[0] == 'G' && buffer[1] == 'X')
                    {
                        sz = buffer[2];

                        if( buffer[3+sz+0] == 'B' && buffer[3+sz+1] == 'T')
                        {
                            byte[] data = new byte[sz+1];
                            System.arraycopy( buffer , 3 , data ,0 ,  sz );
                            data[sz] = 0x00;
//                            GameGirlActivity.GetInstance().StoreReceivedData( data );
                            StoreReceivedData( data );
                        }
                    }

                } catch (IOException e) {
                    //エラー
                    bLoop = false;
                    m_Seq = 1000;
                }

                try {
                    Thread.sleep(1000/120);
                } catch (InterruptedException e) {
                    //Log.e(TAG, "Exception during write", e);
                }


            }
        }
    }

}
