﻿#ifndef CFILETEXT_H_
#define CFILETEXT_H_

class CFileText
{
public:
	enum {
		eTypeSJIS,
		eTypeUTF8,
		eTypeUTF16,
	};

	CFileText()
	{
		m_uStringMaxSize = 128;
		m_pBuf = new gxChar[1024];

		m_sLineNum = 0;

		m_sTypeIn = eTypeSJIS;
		m_sTypeOut= eTypeUTF8;

	}
	
	~CFileText()
	{
		SAFE_DELETES(m_pBuf);
	}

	void AddLine(gxChar* pStr, ...)
	{
		gxChar temp[1024];

		va_list app;

		va_start(app, pStr);
		vsprintf(temp, pStr, app );
		//vswprintf((wchar_t*)s_StrTemp, enTempLength, (wchar_t*)pStr, app);
		va_end(app);

		gxChar* str = temp;
		size_t sz = strlen(str);

		if (m_uStringSize+sz >= m_uStringMaxSize)
		{
			//確保していたメモリをオーバーしたら以前の2倍のバッファを確保し直す
			m_uStringMaxSize *= 2;
			gxChar* pBuf = new gxChar[m_uStringMaxSize + 1];
			gxUtil::MemCpy(pBuf, m_pBuf , m_uStringSize );
			SAFE_DELETES(m_pBuf);
			m_pBuf = pBuf;
			m_pBuf[m_uStringSize] = 0x00;
		}

		size_t sz2 = sprintf( &m_pBuf[m_uStringSize], "%s\n", str);
		m_uStringSize += sz2;
		m_sLineNum++;
	}

	void SaveFile(gxChar* pFileName)
	{
		//ファイルを書き出す

		if (m_sTypeIn == eTypeSJIS )
		{
			if (m_sTypeOut == eTypeUTF8)
			{
				size_t sz_u16;
				size_t sz_u8;
				wchar_t* pU16 = CDeviceManager::SJIStoUTF16( m_pBuf, &sz_u16 );
				gxChar*  pU8  = CDeviceManager::UTF16toUTF8( pU16, &sz_u8 );

				SAFE_DELETES( m_pBuf);
				m_pBuf = pU8;
				m_uStringSize = sz_u8;
			}
		}

		gxLib::SaveFile(pFileName , (Uint8*)m_pBuf, m_uStringSize );
	}

	gxChar* GetTextBuffer(size_t* sz = NULL)
	{
		if (sz)* sz = m_uStringSize;
		return m_pBuf;
	}

private:

	Uint32 m_uStringSize = 0;
	Uint32 m_uStringMaxSize = 0;
	gxChar * m_pBuf;
	Uint32 m_sLineNum;

	Sint32 m_sTypeIn;
	Sint32 m_sTypeOut;
};

#endif
