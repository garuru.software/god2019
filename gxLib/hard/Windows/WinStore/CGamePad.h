﻿//----------------------------------------------------
//ハードウェア的なパッド情報
//----------------------------------------------------
#ifndef _CGAMEPAD_H_
#define _CGAMEPAD_H_

typedef struct PadStat_t {
	Float32 x,y,z;
}PadStat_t;

enum GAMINGDEVICE_BTNID{
	//ゲーミングデバイスの割り当て定義
	GDBTN_NONE = -1,
	GDBTN_BUTTON00=0,
	GDBTN_BUTTON01,
	GDBTN_BUTTON02,
	GDBTN_BUTTON03,
	GDBTN_BUTTON04,
	GDBTN_BUTTON05,
	GDBTN_BUTTON06,
	GDBTN_BUTTON07,
	GDBTN_BUTTON08,
	GDBTN_BUTTON09,
	GDBTN_BUTTON10,
	GDBTN_BUTTON11,
	GDBTN_BUTTON12,
	GDBTN_BUTTON13,
	GDBTN_BUTTON14,
	GDBTN_BUTTON15,

	GDBTN_AXIS_XA,
	GDBTN_AXIS_XS,

	GDBTN_AXIS_YA,
	GDBTN_AXIS_YS,

	GDBTN_AXIS_ZA,
	GDBTN_AXIS_ZS,

	GDBTN_ROT_XA,
	GDBTN_ROT_XS,

	GDBTN_ROT_YA,
	GDBTN_ROT_YS,

	GDBTN_ROT_ZA,
	GDBTN_ROT_ZS,

	GDBTN_POV0_U,
	GDBTN_POV0_R,
	GDBTN_POV0_D,
	GDBTN_POV0_L,

	GDBTN_POV1_U,
	GDBTN_POV1_R,
	GDBTN_POV1_D,
	GDBTN_POV1_L,

	GDBTN_POV2_U,
	GDBTN_POV2_R,
	GDBTN_POV2_D,
	GDBTN_POV2_L,

	GDBTN_POV3_U,
	GDBTN_POV3_R,
	GDBTN_POV3_D,
	GDBTN_POV3_L,

	GDBTN_SLIDER0_U,
	GDBTN_SLIDER0_D,

	GDBTN_SLIDER1_U,
	GDBTN_SLIDER1_D,

	DUMMY1=63,
	GDBTN_MAX,
};

class CGameingDevice
{
public:
	enum {
		JOYMAX = (16),
	};

	CGameingDevice();
	~CGameingDevice();

	Sint32 		_id;
	gxChar 		_hardwarename[FILENAMEBUF_LENGTH];	//機器の名前
	PadStat_t 	_caribrate;
	PadStat_t 	_caribrateR;
	gxBool 		_button[128];		//ボタン入力（USBパッドのボタン１２８個分）
	PadStat_t	_Axis;				//アナログスティック値
	PadStat_t	_Rotation;			//アナログスティック値
	Sint32 		_Pov0;				//アナログスティック値
	Sint32 		_Pov1;				//アナログスティック値
	Sint32 		_Pov2;				//アナログスティック値
	Sint32 		_Pov3;				//アナログスティック値
	Float32		_Slider0;			//アナログスティック値
	Float32		_Slider1;			//アナログスティック値

	//------------------------------------------------------
	//サポート状況
	//------------------------------------------------------
	Sint32 _num_pov;					//ハットスイッチ数
	Sint32 _num_slider;					//スライダーコントロール数
	gxBool _bPov[4];					//ハットスイッチ
	gxBool _bAxisX,_bAxisY,_bAxisZ;		//サポートされているAXIS
	gxBool _bAxisRX,_bAxisRY,_bAxisRZ;	//サポートされているAXIS
	gxBool _bSlider[2];					//サポートされているSLIDER
	gxBool _bRumble;					//振動可能か？
	Sint32 _ForceFeefBackAxis;			//フォースフィードバックの軸数

	//-------------------------------------------------------
	//各軸の使用の可否
	//-------------------------------------------------------
	gxBool _bUseAxisX;
	gxBool _bUseAxisY;
	gxBool _bUseAxisZ;
	gxBool _bUseRotationX;
	gxBool _bUseRotationY;
	gxBool _bUseRotationZ;
	gxBool _bUsePOV0;
	gxBool _bUsePOV1;
	gxBool _bUsePOV2;
	gxBool _bUsePOV3;
	gxBool _bUseSlider0;
	gxBool _bUseSlider1;
	gxBool _bUseForceFeedBack;

private:
	gxBool IsPress( GAMINGDEVICE_BTNID id );
	gxBool IsPressButton (gxBool& btn);
	gxBool IsPressAxis   (Float32& axis,Sint32 dir);
	gxBool IsPressPov    (Sint32 &pov ,Sint32 urdl);
	gxBool IsPressSlider (Float32 &sldr,Sint32 dir);
};


class CGamePad
{
public:
	enum {
		enDeviceMax = 64,
	};

	CGamePad();

	~CGamePad()
	{
	}

	void Init();
	void Action();

	CGameingDevice* GetDevice()
	{
		m_DeviceCnt++;
		return &m_Device[m_DeviceCnt-1];
	}

	//Sint32 ConvertKeyNumber( WPARAM id );
	//void   InputKeyCheck( UINT iMsg , WPARAM wParam ,LPARAM lParam );

	void SetMousePosition( Sint32 x , Sint32 y )
	{
		m_MouseX = x;
		m_MouseY = y;
	}

	//デフォルトのコントローラー設定
	void AdjustDefaultInput();

	//コントローラーコンフィグ画面
	void ControllerConfig();
	gxBool waitButtonDown();

	void SetKeyDown( Sint32 key )
	{
		m_KeyBoardPush[ key ] = 0x01;
	}

	void SetKeyUp(Sint32 key )
	{
		m_KeyBoardPush[ key ] = 0x02;
	}

	SINGLETON_DECLARE( CGamePad );

private:

	void drawDevice( Sint32 id , Sint32 ax , Sint32 ay);

	Sint32 m_DeviceCnt;
	CGameingDevice m_Device[enDeviceMax];

	//キーボード
	Uint8 m_KeyBoardPush[256];

	//マウス
	Uint8   m_MouseClick[8];
	Float32 m_MouseX,m_MouseY;
	Float32 m_Wheel;
};

#endif

