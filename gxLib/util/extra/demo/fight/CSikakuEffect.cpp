﻿//------------------------------------------------
//
// エフェクトルーチン
// 
// written by tomi.2013.01.18
//
//------------------------------------------------
#include <gxLib.h>
#include "sikaku.h"

CSikakuEffect::CSikakuEffect( Sint32 x , Sint32 y , Sint32 dir , Sint32 sPlayer , gxBool bFinish )
{
	//------------------------------------------------
	//------------------------------------------------
	
	m_Pos.x = x;
	m_Pos.y = y;

	m_Add.x = dir*400;
	m_Add.y = 0;

	m_sPlayer = sPlayer;
	m_bFinish = bFinish;

	m_sDir = 0;

	m_sWait = 0;
}


CSikakuEffect::~CSikakuEffect()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CSikakuEffect::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------
	
	if( CSikakuGameMain::GetInstance()->IsTheWorld( ) )
	{
		return;
	}

	if( m_Kurai.IsHit() )
	{
		new CEffHitmark( m_Pos.x , m_Pos.y );
		SetActionSeq( enActionSeqCrash );
		return;
	}

	if( m_Atari.IsHit() )
	{
		new CEffHitmark( m_Pos.x , m_Pos.y );
		SetActionSeq( enActionSeqCrash );
		return;
	}

	setAtari( -16,-16,32,32 );
	
	CSikakuGameMain::GetInstance()->SetTobidougu( m_sPlayer );


	m_Pos.x += m_Add.x;
	m_Pos.y += m_Add.y;

	if( gxLib::Rand()%4 == 0 )
	{
		new CEffBeam( m_Pos.x , m_Pos.y );
	}

	if( m_Pos.x >= (enStageWidth+128)*100 || m_Pos.x < (0-128)*100 )
	{
		SetActionSeq( enActionSeqEnd );
	}

	EnCollisionID id;
	if( m_sPlayer == 0 )
	{
		id = enCollisionID_A_Defence;
	}
	else
	{
		id = enCollisionID_B_Defence;
	}

	m_Kurai.On();
	m_Kurai.Set( id , m_Pos.x/100-16 , m_Pos.y/100-16 , 32 , 32 );

}


void CSikakuEffect::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------
	m_Atari.Off();

	if( m_sWait > 8 )
	{
		SetActionSeq( enActionSeqEnd );
	}
	else
	{
		for(Sint32 ii=0;ii<8;ii++)
		{
			new CEffPlasma( m_Pos.x , m_Pos.y );
		}
	}

	m_sWait ++;
}


void CSikakuEffect::setAtari( Sint32 u , Sint32 v , Sint32 w, Sint32 h )
{
	EnCollisionID id;
	Sint32 sx = m_Pos.x/100;
	Sint32 sy = m_Pos.y/100;
	Sint32 sw=0,sh=0;

	m_Atari.m_sHp = 100;
	m_Atari.m_sAtDir = m_sDir;

	if( m_bFinish )
	{
		m_Atari.m_sAtEff = 1;
	}

	if( m_sPlayer == 0 )
	{
		id = enCollisionID_A_Atack;
	}
	else
	{
		id = enCollisionID_B_Atack;
	}

	if( m_sDir == DIR_RIGHT )
	{
		sx = m_Pos.x/100 + u;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	else
	{
		sx = m_Pos.x/100 - u - w;
		sy = m_Pos.y/100 + v;
		sw = w;
		sh = h;
	}
	m_Atari.On();
	m_Atari.Set( id , sx , sy , sw , sh );

}


void CSikakuEffect::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;

	dx = m_Pos.x;
	dy = m_Pos.y;

	CDraw::PutSprite(dx,dy,64,enTexMain,0,0,16,16,8,8,ATR_DFLT, 0xff00ffff );
	
}
















CEffHitmark::CEffHitmark( Sint32 x , Sint32 y )
{
	//------------------------------------------------
	//------------------------------------------------
	
	m_Pos.x = x;	
	m_Pos.y = y;

	m_Add.x = 0;
	m_Add.y = 0;

	m_sAlpha = 255;

	m_fScale = 1.f;

	for(Sint32 ii=0;ii<6;ii++)
	{
		new CEffPlasma( m_Pos.x , m_Pos.y );
	}

}


CEffHitmark::~CEffHitmark()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CEffHitmark::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------

	if( CSikakuGameMain::GetInstance()->IsTheWorld( ) )
	{
		return;
	}

	m_Pos.x += m_Add.x;
	m_Pos.y += m_Add.y;

	m_sAlpha -= 16;
	m_fScale += 0.05f;

	if( m_sAlpha < 0 )
	{
		m_sAlpha = 0x00;
		SetActionSeq( enActionSeqCrash );
	}

}


void CEffHitmark::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------

	SetActionSeq( enActionSeqEnd );

}


void CEffHitmark::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;

	dx = m_Pos.x;
	dy = m_Pos.y;

	CDraw::PutSprite(dx,dy,64,enTexMain,0,0,24,24,12,12 , ATR_DFLT , ARGB(m_sAlpha , 0xff,0xff, 0x00 ) , 0 , m_fScale , m_fScale );
	
}











CEffBeam::CEffBeam( Sint32 x , Sint32 y )
{
	//------------------------------------------------
	//------------------------------------------------
	
	m_Add.x = x;
	m_Add.y = y;

	Sint32 r = gxLib::Rand()%360;

	m_Pos.x = x + gxUtil::Cos(r)*800;
	m_Pos.y = y + gxUtil::Sin(r)*800;

	m_sAlpha = 255;

	m_fScale = 1.f;
}


CEffBeam::~CEffBeam()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CEffBeam::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------

	m_Pos.x += ( m_Add.x - m_Pos.x )/10;
	m_Pos.y += ( m_Add.y - m_Pos.y )/10;

	m_sAlpha -= 16;
	m_fScale += 0.05f;

	if( m_sAlpha < 0 )
	{
		m_sAlpha = 0x00;
		SetActionSeq( enActionSeqCrash );
	}

}


void CEffBeam::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------

	if( gxLib::Rand()%3 == 0 )
	{
		new CEffBeam( m_Add.x , m_Add.y );
	}

	SetActionSeq( enActionSeqEnd );

}


void CEffBeam::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;

	dx = m_Pos.x;
	dy = m_Pos.y;

	CDraw::PutSprite(dx,dy,64,enTexMain,0,0,24,24,12,12 , ATR_ALPHA_PLUS , ARGB(m_sAlpha , 0xff,0xff, 0x00 ) , 0 , m_fScale , m_fScale );
	
}














CEffPlasma::CEffPlasma( Sint32 x , Sint32 y )
{
	//------------------------------------------------
	//------------------------------------------------
	
	m_Pos.x = x;
	m_Pos.y = y;

	Sint32 r = gxLib::Rand()%360;

	m_Add.x = gxUtil::Cos(r)*3200;
	m_Add.y = gxUtil::Sin(r)*3200;

	m_sAlpha = 255;

	m_fScale = 1.f;
}


CEffPlasma::~CEffPlasma()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CEffPlasma::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------

	m_Pos.x += m_Add.x/10;
	m_Pos.y += m_Add.y/10;

	m_sAlpha -= 16;
	m_fScale += 0.05f;

	if( m_sAlpha < 0 )
	{
		m_sAlpha = 0x00;
		SetActionSeq( enActionSeqCrash );
	}

}


void CEffPlasma::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------

/*
	if( gxLib::Rand()%3 == 0 )
	{
		new CEffPlasma( m_Add.x , m_Add.y );
	}
*/
	SetActionSeq( enActionSeqEnd );

}


void CEffPlasma::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;

	dx = m_Pos.x;
	dy = m_Pos.y;

	CDraw::PutSprite(dx,dy,48,enTexMain,0,0,24,24,12,12 , ATR_ALPHA_PLUS , ARGB(m_sAlpha , 0x00,0xff, 0x00 ) , 0 , m_fScale/2 , m_fScale/2 );
	
}









CEffComboDisp::CEffComboDisp( Sint32 pl , Sint32 num )
{
	//------------------------------------------------
	//------------------------------------------------
	
	if( pl == 0 )
	{
		m_Add.x = 32*100;
		m_Pos.x = m_Add.x - 640*100;
	}
	else
	{
		m_Add.x = ((WINDOW_W/2)+128)*100;
		m_Pos.x = m_Add.x + 640*100;
	}

	if( num < 0 )
	{
		m_sType = num*-1;
	}
	else
	{
		m_sType = 0;
	}

	m_Pos.y = 64*100;
	m_Add.y = 64*100;
	m_Add.z = num;

	m_sAlpha = 255;

	m_fScale = 1.f;
}


CEffComboDisp::~CEffComboDisp()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CEffComboDisp::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------

	m_Pos.x += (m_Add.x-m_Pos.x)/10;

	m_sAlpha -= 3;

	if( m_sAlpha < 0 )
	{
		m_sAlpha = 0x00;
		SetActionSeq( enActionSeqCrash );
	}

}


void CEffComboDisp::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------

/*
	if( gxLib::Rand()%3 == 0 )
	{
		new CEffComboDisp( m_Add.x , m_Add.y );
	}
*/
	SetActionSeq( enActionSeqEnd );

}


void CEffComboDisp::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;

	dx = m_Pos.x/100;
	dy = m_Pos.y/100+24;

	if( m_sType == 0 )
	{
		gxLib::Printf( dx , dy , 128, ATR_DFLT , ARGB_DFLT , "%d HIT COMBO!" , m_Add.z );
	}
	else if( m_sType == 1 )
	{
		gxLib::Printf( dx , dy+16 , 128, ATR_DFLT , ARGB_DFLT , "First Atack!" );
	}
	else if( m_sType == 2 )
	{
		gxLib::Printf( dx , dy+24 , 128, ATR_DFLT , ARGB_DFLT , "Reversal Atack!" );
	}
	
}


CEffAtack::CEffAtack( Sint32 x , Sint32 y , Sint32 sType )
{
	//------------------------------------------------
	//------------------------------------------------
	
	m_Pos.x = x;
	m_Pos.y = y;

	Sint32 r = 0;

	m_Add.x = 0;
	m_Add.y = 0;

	m_sAlpha = 255;

	m_fScale = 1.f;

	m_sType = sType;
}


CEffAtack::~CEffAtack()
{
	//------------------------------------------------
	//------------------------------------------------
	
	
}


void CEffAtack::SeqMain()
{
	//------------------------------------------------
	//------------------------------------------------

	m_Pos.x += m_Add.x/10;
	m_Pos.y += m_Add.y/10;

	m_sAlpha -= 16;
	m_fScale += 0.05f;

	if( m_sAlpha < 0 )
	{
		m_sAlpha = 0x00;
		SetActionSeq( enActionSeqCrash );
	}

}


void CEffAtack::SeqCrash()
{
	//------------------------------------------------
	//------------------------------------------------

/*
	if( gxLib::Rand()%3 == 0 )
	{
		new CEffAtack( m_Add.x , m_Add.y );
	}
*/
	SetActionSeq( enActionSeqEnd );

}


void CEffAtack::Draw()
{
	//------------------------------------------------
	//------------------------------------------------
	
	Sint32 dx,dy;
	Uint32 argb = 0xffffffff;

	dx = m_Pos.x;
	dy = m_Pos.y;

	if( m_sType == 1 )
	{
		//ピヨリ
		argb = ARGB(m_sAlpha , 0xff,0xff, 0x00 );
	}
	else
	{
		//攻撃
		argb = ARGB(m_sAlpha , 0x00,0x00, 0xf0 );
	}

	CDraw::PutSprite(dx,dy,48,enTexMain,0,0,24,24,12,12 , ATR_DFLT , argb , 0 , m_fScale/2 , m_fScale/2 );
	
}


