﻿//-------------------------------------------------
//行列計算
//-------------------------------------------------

#include <gxLib.h>
#include "gxMatrix.h"

void mtxSetUnit(void);
void mtxSetUnit2(MATRIX *m);
void mtxTrans(VECTOR3 *v);
void mtxRotZ(float r);
void mtxRotX(float r);
void mtxRotY(float r);
void mtxScale(float x, float y, float z);
void mtxAffin2(VECTOR3 *d, MATRIX *m, VECTOR3 *s);
void mtxAffin(VECTOR3 *d, VECTOR3 *s);
void mtxAffinLocal(VECTOR3 *d, VECTOR3 *s);

void vctAdd(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctSub(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctMul(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void vctDiv(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2);
void mtxMul(MATRIX *m2,MATRIX *m1);


static MATRIX	unitmtx={
	//単位行列
	1.0f,0.0f,0.0f,0.0f,
	0.0f,1.0f,0.0f,0.0f,
	0.0f,0.0f,1.0f,0.0f,
	0.0f,0.0f,0.0f,1.0f,
};

MATRIX cm;	//カレントマトリックス

MATRIX GetCurrentMatrix()
{
	return cm;
}

void SetCurrentMatrix(MATRIX *m)
{
	cm = *m;
}

void mtxSetUnit(void)
{
	cm = unitmtx;
}

void mtxSetUnit2(MATRIX *m)
{
	*m = unitmtx;
}


void mtxTrans(VECTOR3 *v)
{
	MATRIX	m;

	m = unitmtx;

	m._41 = v->x;
	m._42 = v->y;
	m._43 = v->z;

	mtxMul(&cm,&m);

}

void mtxRotZ(float r)
{
	MATRIX	m;
	float	s,c;

	s = (float)sin(r);
	c = (float)cos(r);

	m = unitmtx;

	m._11 = c;
	m._12 = -s;

	m._21 = s;
	m._22 = c;

	mtxMul(&cm,&m);
}

void mtxRotX(float r)
{
	MATRIX	m;
	float	s,c;

	s = (float)sin(r);
	c = (float)cos(r);

	m = unitmtx;

	m._22 = c;
	m._23 = -s;

	m._32 = s;
	m._33 = c;

	mtxMul(&cm,&m);

}

void mtxRotY(float r)
{
	MATRIX	m;
	float	s,c;

	s = (float)sin(r);
	c = (float)cos(r);

	m = unitmtx;

	m._11 = c;
	m._13 = s;

	m._31 = -s;
	m._33 = c;

	mtxMul(&cm,&m);

}
void mtxScale(float x, float y, float z)
{
	MATRIX	m;

	m = unitmtx;

	m._11 = x;
	m._22 = y;
	m._33 = z;

	mtxMul(&cm,&m);
}


void mtxAffin2(VECTOR3 *d, MATRIX *m, VECTOR3 *s)
{
	VECTOR3	v;
	v.x = s->x*m->_11 + s->y*m->_21 + s->z*m->_31 + m->_41 ;
	v.y = s->x*m->_12 + s->y*m->_22 + s->z*m->_32 + m->_42 ;
	v.z = s->x*m->_13 + s->y*m->_23 + s->z*m->_33 + m->_43 ;
	*d =v;
}

void mtxAffin(VECTOR3 *d, VECTOR3 *s)
{
	VECTOR3	v;
	v.x = s->x*cm._11 + s->y*cm._21 + s->z*cm._31 + cm._41 ;
	v.y = s->x*cm._12 + s->y*cm._22 + s->z*cm._32 + cm._42 ;
	v.z = s->x*cm._13 + s->y*cm._23 + s->z*cm._33 + cm._43 ;


	v.x = v.x/(10.0f-v.z);
	v.y = v.y/(10.0f-v.z);
	*d =v;
}

void mtxAffinLocal(VECTOR3 *d, VECTOR3 *s)
{
	VECTOR3	v;
	v.x = s->x*cm._11 + s->y*cm._21 + s->z*cm._31 ;
	v.y = s->x*cm._12 + s->y*cm._22 + s->z*cm._32 ;
	v.z = s->x*cm._13 + s->y*cm._23 + s->z*cm._33 ;
	*d =v;
}

#define	CAMKAKU		(65.0f)
#define	Z_RATE		(10.0f)
void TransPers(VECTOR3 *v)
{
//	Float siya = (Float)tan(CAMKAKU/2/180*3.1416)*2.f;
//
//	v->x = (Float)( SCREEN_H*v->x/(v->z*siya)+SCREEN_W/2);
//	v->y = (Float)(-SCREEN_H*v->y/(v->z*siya)+SCREEN_H/2);
//
////	v->rhw = 1/v->z;
//
//	Float ZrateR = (1/Z_RATE);
//	v->z = v->z*ZrateR;

	v->x = v->x*Z_RATE;//v->z);
	v->y = v->y*Z_RATE;//v->z);
}

void mtxcpy(MATRIX *d)
{
	*d = cm;
}

void vctUnit(VECTOR3 *d, VECTOR3 *s)
{
	float	l;

	*d = *s;
	l = ((float)1)/(float)sqrt(s->x*s->x + s->y*s->y + s->z*s->z);
	d->x *= l;
	d->y *= l;
	d->z *= l;
}

// 内積
float vctInp(VECTOR3 *v1, VECTOR3 *v2)
{
	return (float)(v1->x*v2->x + v1->y*v2->y + v1->z*v2->z);
}

// 外積
void vctOutp(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2)
{
	d->x = v1->y * v2->z - v1->z * v2->y;
	d->y = v1->z * v2->x - v1->x * v2->z;
	d->z = v1->x * v2->y - v1->y * v2->x;
}

//------------------------------------------------
//行列計算
//------------------------------------------------
void vctPolyNorm(VECTOR3 *vd,VECTOR3 *v1,VECTOR3 *v2,VECTOR3 *v3)
{
	VECTOR3	t1,t2;

	vctSub(&t1,v2,v1);
	vctSub(&t2,v3,v1);
	vctOutp(vd,&t1,&t2);
	vctUnit(vd,vd);
}

void vctAdd(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2)
{
	d->x = v1->x + v2->x;
	d->y = v1->y + v2->y;
	d->z = v1->z + v2->z;
}

void vctSub(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2)
{
	d->x = v1->x - v2->x;
	d->y = v1->y - v2->y;
	d->z = v1->z - v2->z;
}

void vctMul(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2)
{
	d->x = v1->x * v2->x;
	d->y = v1->y * v2->y;
	d->z = v1->z * v2->z;
}

void vctDiv(VECTOR3 *d, VECTOR3 *v1, VECTOR3 *v2)
{
	d->x = v1->x / v2->x;
	d->y = v1->y / v2->y;
	d->z = v1->z / v2->z;
}

void mtxMul(MATRIX *m2,MATRIX *m1)
{
	MATRIX	m;

	m._11 = (m1->_11*m2->_11) + (m1->_12*m2->_21) + (m1->_13*m2->_31) + (m1->_14*m2->_41) ;
	m._12 = (m1->_11*m2->_12) + (m1->_12*m2->_22) + (m1->_13*m2->_32) + (m1->_14*m2->_42) ;
	m._13 = (m1->_11*m2->_13) + (m1->_12*m2->_23) + (m1->_13*m2->_33) + (m1->_14*m2->_43) ;
	m._14 = (m1->_11*m2->_14) + (m1->_12*m2->_24) + (m1->_13*m2->_34) + (m1->_14*m2->_44) ;

	m._21 = (m1->_21*m2->_11) + (m1->_22*m2->_21) + (m1->_23*m2->_31) + (m1->_24*m2->_41) ;
	m._22 = (m1->_21*m2->_12) + (m1->_22*m2->_22) + (m1->_23*m2->_32) + (m1->_24*m2->_42) ;
	m._23 = (m1->_21*m2->_13) + (m1->_22*m2->_23) + (m1->_23*m2->_33) + (m1->_24*m2->_43) ;
	m._24 = (m1->_21*m2->_14) + (m1->_22*m2->_24) + (m1->_23*m2->_34) + (m1->_24*m2->_44) ;

	m._31 = (m1->_31*m2->_11) + (m1->_32*m2->_21) + (m1->_33*m2->_31) + (m1->_34*m2->_41) ;
	m._32 = (m1->_31*m2->_12) + (m1->_32*m2->_22) + (m1->_33*m2->_32) + (m1->_34*m2->_42) ;
	m._33 = (m1->_31*m2->_13) + (m1->_32*m2->_23) + (m1->_33*m2->_33) + (m1->_34*m2->_43) ;
	m._34 = (m1->_31*m2->_14) + (m1->_32*m2->_24) + (m1->_33*m2->_34) + (m1->_34*m2->_44) ;

	m._41 = (m1->_41*m2->_11) + (m1->_42*m2->_21) + (m1->_43*m2->_31) + (m1->_44*m2->_41) ;
	m._42 = (m1->_41*m2->_12) + (m1->_42*m2->_22) + (m1->_43*m2->_32) + (m1->_44*m2->_42) ;
	m._43 = (m1->_41*m2->_13) + (m1->_42*m2->_23) + (m1->_43*m2->_33) + (m1->_44*m2->_43) ;
	m._44 = (m1->_41*m2->_14) + (m1->_42*m2->_24) + (m1->_43*m2->_34) + (m1->_44*m2->_44) ;

	*m2 = m;

}

