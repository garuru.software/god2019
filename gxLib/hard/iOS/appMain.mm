//ファイルローダー

#import <Foundation/Foundation.h>
#include <gxLib.h>
#include <gxLib/gx.h>
#import <unistd.h>
#import <sys/stat.h>
#include "appMain.h"
#include "CGamePad.h"
#include "COpenAL.h"

SINGLETON_DECLARE_INSTANCE( CiOS );

void AppInit()
{
    CDeviceManager::GetInstance()->AppInit();
    
}


void AppUpdate()
{
    CGameGirl::GetInstance()->Action();
    
    if (CGameGirl::GetInstance()->IsAppFinish())
    {
        
    }
    
}


void AppFlip()
{
    
    
}


void AppFinish()
{
    CGameGirl::DeleteInstance();
    COpenAL::DeleteInstance();
    CDeviceManager::DeleteInstance();
    gxLib::Destroy();
}




bool CreateDirectories(char* pURL )
{
    gxBool bSuccess = gxTrue;
    
    std::vector<char*> separates;
    std::string url = pURL;;
    std::replace(url.begin(), url.end(), '\\', '/' );
    
    char *pBuf = new char[ url.size()+1];
    sprintf( pBuf, "%s", url.c_str());
    
    Sint32 cnt = 0;
    
    for ( Sint32 ii= url.size(); ii>=0; ii-- )
    {
        if ( pBuf[ii] == ':')
        {
            separates.push_back(&pBuf[0]);
            cnt++;
            break;
        }
        else if ( ii == 0 )
        {
            separates.push_back(&pBuf[0]);
            cnt++;
            break;
        }
        else if ( pBuf[ii] == '/')
        {
            pBuf[ii] = 0x00;
            if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
            cnt++;
        }
    }
    
    size_t max = separates.size();
    std::string path = "";
    
    for (Sint32 ii = 0; ii < max; ii++)
    {
        path += separates[max - 1 - ii];
        
        if (!mkdir( path.c_str(), S_IRWXU ))
        {
            bSuccess = gxFalse;
        }
        
        path += "/";
    }
    
    delete[] pBuf;
    
    return bSuccess;
}


gxChar* GetDocumentPath(char dst[], unsigned dstSize)
{
    NSArray*      dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*     dir  = [dirs objectAtIndex:0];
    
    const char* src  = [dir UTF8String];
    unsigned    len  = strlen(src);
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}

gxChar* GetLibraryPath(char dst[], unsigned dstSize)
{
    NSArray*      dirs = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString*     dir  = [dirs objectAtIndex:0];
    
    const char* src  = [dir UTF8String];
    unsigned    len  = strlen(src);
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}



gxChar* GetResourcePath(char dst[], unsigned dstSize)
{
    NSString *  res  = [[NSBundle mainBundle] pathForResource:@"PkgInfo" ofType:nil];
    const char* src  = [res UTF8String];
    unsigned    len  = strlen(src);
    if (len > 7)
        len -= 7;
    if (len >  dstSize - 1) {
        assert(0);
        len  = dstSize - 1;
    }
    memcpy(dst, src, len);
    dst[len] = 0;
    
    return dst;
}


size_t getFileSize(const char* path)
{
    FILE *p = NULL;
    p = fopen( path, "rb" );
    if ( p ) {
        size_t size;
        fseek( p, 0, SEEK_END );
        size = ftell( p );
        fseek( p, 0, SEEK_SET );
        fclose( p );
        return size;
    }
    return 0;
}


void* _loadFile(const char* name, size_t* pLength)
{
    
    Uint32 len = strlen( name );
    
    gxChar fileName0[512];
    
    len = sprintf(fileName0, "%s", name);
    
    //zip時は / でパスを区切る
    
    for (int ii = 0; ii<len; ii++)
    {
        if (fileName0[ii] == '\\')
        {
            fileName0[ii] = '/';
        }
    }

    name = fileName0;
    
    if (pLength)
        *pLength = 0;
    size_t    size = getFileSize(name);
    if (size == 0)
        return 0;
    size_t    alloc_size = (size + 1 + 15) & ~15;    // 1～16バイト余分に確保.
//    void* buf = ::malloc( alloc_size );
    Uint8* buf = new Uint8[alloc_size ];
    if (buf == 0) {
        return 0;
    }

    FILE*    fp = fopen(name, "rb");
    if (fp == 0)
    {
        //::free(buf);
        delete[] buf;
        *pLength = 0;
        return nullptr;
    }

    size_t n = 0;
    n = fread(buf, 1, size, fp);
    fclose(fp);

    if (pLength)
    {
        *pLength = size;
    }

    return buf;
}


size_t _saveFile(const char* name, const void* buf, unsigned size)
{
    assert(name != 0);
    assert(buf != 0);
    assert(size > 0);
#if 1
    FILE*    fp = fopen(name, "wb");
    if (fp == 0)
    {
        CreateDirectories((char*)name);
        fp = fopen(name, "wb");
        if (fp == 0)
        {
            return 0;
        }
    }
    
    size_t n = fwrite(buf, 1, size, fp);
    fclose(fp);
    return n;
#else
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSData* data = [NSData dataWithBytes:buf length:size];
    NSString* key = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
    return 0;
#endif
}

