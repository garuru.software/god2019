﻿#ifndef _GXNETORKMANAGER_H_
#define _GXNETORKMANAGER_H_

//ネットワーク管理
#include <iostream>
#include <map>

class gxNetworkManager
{
public:
	//enum {
	//	enHttpOpenMax = 1024,
	//};

	typedef struct StHTTP
	{
		StHTTP()
		{
			m_URL[0] = 0x00;
			pData = NULL;
			bRequest = gxFalse;
			m_User[0] = 0x00;
			m_Pass[0] = 0x00;
			m_Seq = 0;
			uFileSize = 0;
			uReadFileSize = 0;
			index = 0;
			bClose = gxTrue;
		}

		void SetSeq( Sint32 seq )
		{
			m_Seq = seq;
		}
		
		Sint32 GetSeq()
		{
			return m_Seq;
		}

		gxBool IsNowLoading()
		{
			if( m_Seq == 0 || m_Seq == 999 ) return gxFalse;
			return gxTrue;
		}

		gxChar m_URL[1024];
		gxChar m_User[128];
		gxChar m_Pass[128];
		Uint8 *pData;
		gxBool bRequest;
		size_t uFileSize;
		size_t uReadFileSize;
		Sint32 m_Seq;
		Sint32 index;
		gxBool bClose = gxFalse;

	} StHTTP;

	gxNetworkManager()
	{
		//m_pHttp = new StHTTP[ enHttpOpenMax ];
		m_sHttpRequestMax = 0;
	}

	~gxNetworkManager()
	{
		//SAFE_DELETES( m_pHttp );
	}

	void Action();

	Sint32 OpenURL( gxChar const *pOpenURL );
	gxBool CloseURL(Uint32 uIndex);

	StHTTP* GetHTTPInfo( Uint32 uIndex )
	{
		return &m_pHttp[ uIndex ];
	}

	StHTTP* GetNextReq();

	SINGLETON_DECLARE( gxNetworkManager );

private:

	std::map<int , StHTTP> m_pHttp;
	Sint32 m_sHttpRequestMax;
};

#endif
