﻿//-------------------------------------------------
//行列計算
//-------------------------------------------------

#include <gxLib.h>
#include "gxUIManager.h"
#include "gxMatrix.h"

SINGLETON_DECLARE_INSTANCE(gxUIManager);

void gxUIManager::SetDialog( gxBool bEndless , gxChar *pCaption , gxChar *pString , ... )
{
	//Dialogを表示する

	va_list app;
	va_start(app, pString);
	vsprintf(m_MessageString, pString, app );
	va_end(app);

	sprintf( m_CaptionString , "%s" , pCaption );

	m_fDialogTime = 2.0f;
	m_fDialogAddTime = -1.0f / 120.0f;

	if( bEndless )
	{
		//m_fDialogAddTime = 0.0f;
	}

}


void gxUIManager::Action()
{
	m_fFadeNow += m_fFadeSpeed;

	if (m_fFadeNow <= 0.0f)
	{
		m_fFadeNow = 0.0f;
		m_FadeDir = 0;
		m_fFadeSpeed = 0.0f;
	}
	else if (m_fFadeNow >= 1.0f)
	{
		m_fFadeNow = 1.0f;
		m_FadeDir = 0;
		m_fFadeSpeed = 0.0f;
	}
	else
	{
		//Fade中
		int ct = 0;
		ct++;
	}

	if( m_fNowLoading > 0.0f )
	{
		m_fNowLoading -= 1.0f/60.0f;
	} 
	if( m_fNowSaving > 0.0f )
	{
		m_fNowSaving -= 1.0f/60.0f;
	} 

	if( m_fNowHttpConnecting > 0.0f )
	{
		m_fNowHttpConnecting -= 1.0f / 60.0f;
	} 

	if( m_fNowBlueToothEnable > 0.0f )
	{
		m_fNowBlueToothEnable -= 1.0f / 60.0f;
	} 

	if( m_sVolumeMeterDispCnt > 0 )
	{
		m_sVolumeMeterDispCnt --;
	}

	m_fDialogTime   += m_fDialogAddTime;

	m_fNowLoading         = CLAMP( m_fNowLoading , 0.0f , 120.f );
	m_fNowSaving          = CLAMP( m_fNowSaving , 0.0f , 120.f );
	m_fNowHttpConnecting  = CLAMP( m_fNowHttpConnecting , 0.0f , 120.f );
	m_fNowBlueToothEnable = CLAMP( m_fNowBlueToothEnable, 0.0f , 120.f );
	m_fDialogTime         = CLAMP( m_fDialogTime , 0.0f , 120.0f );


}


void gxUIManager::Draw()
{
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	switch (m_FadeType) {
	case eFadeTypeNormal:
		gxLib::DrawBox(-32, -32, sw + 32, sh + 32, MAX_PRIORITY_NUM , gxTrue , ATR_DFLT , SET_ALPHA( m_fFadeNow , m_FadeARGB ) );

		break;

	case eFadeTypeWipe:
	case eFadeTypeLine:
	case eFadeTypeCircle:
	case eFadeTypeCrossFade:
		break;
	}

	gxChar* prc[]={
		".",
		"..",
		"...",
	};

	gxChar *progress = prc[ (gxLib::GetGameCounter()/8)%3];

	if( m_fNowLoading > 0.0f )
	{
		gxLib::Printf(sw , 32+16*0 , MAX_PRIORITY_NUM , ATR_STR_RIGHT , ARGB_DFLT , "%s Now Loading",progress );
	}

	if( m_fNowSaving > 0.0f )
	{
		gxLib::Printf(sw , 32+16*1 , MAX_PRIORITY_NUM , ATR_STR_RIGHT , ARGB_DFLT , "%s Now Saving",progress );
	}

	if( m_fNowHttpConnecting > 0.0f )
	{
		gxLib::Printf(sw , 32+16*2 , MAX_PRIORITY_NUM , ATR_STR_RIGHT , ARGB_DFLT , "%s Now Connecting",progress );
	}

	if( m_fNowBlueToothEnable > 0.0f )
	{
		gxLib::Printf(sw , 32+16*3 , MAX_PRIORITY_NUM , ATR_STR_RIGHT , ARGB_DFLT , "%s Bluetooth Enable",progress );
	}

	if( m_fDialogTime > 0.0f )
	{
		Float32 fAlpha = m_fDialogTime;
		fAlpha = CLAMP( fAlpha , 0.0f , 1.0f );
		gxLib::DrawBox( -32 , sh/2-64 , sw+32 , sh/2+32 , MAX_PRIORITY_NUM , gxTrue , ATR_ALPHA_MINUS , SET_ALPHA( fAlpha , 0xA0FFFFFF) );
		gxLib::DrawBox( -32 , sh/2-64 , sw+32 , sh/2+32 , MAX_PRIORITY_NUM , gxFalse , ATR_DFLT , SET_ALPHA( fAlpha , 0xFF808080) , 4.0f );
		gxLib::Printf (sw/2 , sh/2-32 , MAX_PRIORITY_NUM , ATR_STR_CENTER , SET_ALPHA( fAlpha , ARGB_DFLT ), "%s" , m_CaptionString );
		gxLib::Printf (sw/2 , sh/2+0  , MAX_PRIORITY_NUM , ATR_STR_CENTER , SET_ALPHA( fAlpha , ARGB_DFLT ), "%s" , m_MessageString );
	}

	if( m_sVolumeMeterDispCnt > 0 || (gxLib::GetAudioMasterVolume()==0.0f) )
	{
		drawVolumeMeter();
	}
}

void gxUIManager::drawVolumeMeter()
{
	Sint32 gw,gh,sw,sh;
	Sint32 prio = MAX_PRIORITY_NUM;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	if( gxLib::GetAudioMasterVolume() == 0.0f )
	{
		gxLib::Printf(0 , 0+16*0 , MAX_PRIORITY_NUM , ATR_STR_LEFT , ARGB_DFLT , "SOUND MUTE" );
		return;
	}

	Float32 fRatio = gxLib::GetAudioMasterVolume();
	gxLib::DrawBox( 0,0 , sw*fRatio , sh/10 , prio , gxTrue , ATR_DFLT , 0xff00ff00 );

}

