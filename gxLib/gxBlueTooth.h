﻿#ifndef _GXBLUETOOTH_H_
#define _GXBLUETOOTH_H_

class CBlueToothManager
{
public:

	enum {
		enBlueToothStoreMax = 64,
        enBlueToothDataBuffeSize = 256,
	};

	CBlueToothManager()
	{
		for( Sint32 ii=0; ii<enBlueToothStoreMax; ii++ )
		{
			pStoreRecvData[ii] = NULL;
			StoreRecvDataSize[ii] = 0;

			pStoreSendData[ii] = NULL;
			StoreSendDataSize[ii] = 0;

            pStoreRecvData[ii] = new Uint8[enBlueToothDataBuffeSize];
            pStoreSendData[ii] = new Uint8[enBlueToothDataBuffeSize];
		}

		StoreSendCnt  = 0;
		StoreWriteCnt = 0;

		StoreRecvCnt  = 0;
		StoreReadCnt  = 0;

		m_bEnable = gxFalse;	//gx的にBlueToothをリクエストしているか？
		m_bExist  = gxTrue;	//実際にBlueToothが有効か？
	}

	~CBlueToothManager()
	{
		for( Sint32 ii=0; ii<enBlueToothStoreMax; ii++ )
		{
			SAFE_DELETES( pStoreRecvData[ii] );
			SAFE_DELETES( pStoreSendData[ii] );
		}
	}

	gxBool IsBlueToothExist()
	{
		return m_bExist;
	}

	gxBool IsEnable()
	{
		return m_bEnable;
	}

	void SetEnable( gxBool bEnable )
	{
        m_bEnable = bEnable;
	}

	void DestroyBlueToothThread()
	{
		m_bExist = gxFalse;
	}


gxBool SendPacket( Uint8* pData , Uint32 uSize)
{
	//BlueToothデータ送信

	Sint32 n = CBlueToothManager::GetInstance()->StoreSendCnt % CBlueToothManager::enBlueToothStoreMax;

    SAFE_DELETES( CBlueToothManager::GetInstance()->pStoreSendData[n] );

	CBlueToothManager::GetInstance()->pStoreSendData[n] = new Uint8[uSize];
	CBlueToothManager::GetInstance()->StoreSendDataSize[n] = uSize;

	gxUtil::MemCpy( (void*)CBlueToothManager::GetInstance()->pStoreSendData[n] , pData , uSize);

	CBlueToothManager::GetInstance()->StoreSendCnt ++;

	return gxTrue;
}


//BlueToothデータ受信
Uint8* ReadPacket( Uint32 *uSize )
{
	Sint32 max = CBlueToothManager::GetInstance()->StoreRecvCnt;
	Sint32 min = CBlueToothManager::GetInstance()->StoreReadCnt;

	if (min >= max)
	{
		*uSize = 0;
		return NULL;
	}

	Sint32 n = CBlueToothManager::GetInstance()->StoreReadCnt%CBlueToothManager::enBlueToothStoreMax;

	CBlueToothManager::GetInstance()->StoreReadCnt ++;
	CBlueToothManager *pBT = CBlueToothManager::GetInstance();

	*uSize = pBT->StoreRecvDataSize[n];
	return pBT->pStoreRecvData[n];
}



	Sint32 StoreSendCnt;
	Sint32 StoreWriteCnt;

	Sint32 StoreRecvCnt;
	Sint32 StoreReadCnt;

	Uint8 *pStoreSendData[enBlueToothStoreMax];
	Uint32 StoreSendDataSize[enBlueToothStoreMax];

	Uint8 *pStoreRecvData[enBlueToothStoreMax];
	Uint32 StoreRecvDataSize[enBlueToothStoreMax];

	SINGLETON_DECLARE( CBlueToothManager );


private:

	gxBool m_bEnable;
	gxBool m_bExist;

};

#endif

