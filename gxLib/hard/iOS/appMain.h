﻿gxChar* GetDocumentPath(char dst[], unsigned dstSize);
gxChar* GetLibraryPath(char dst[], unsigned dstSize);
gxChar* GetResourcePath(char dst[], unsigned dstSize);
void* _loadFile(const char* name, size_t* pLength);
size_t _saveFile(const char* name, const void* buf, unsigned size);




